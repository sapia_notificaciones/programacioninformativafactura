package pe.com.claro.sh.programainformativa.factura.proxy;


import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import pe.com.claro.sh.programainformativa.factura.bean.notificaplantilla.NotificarPlantillaRequest;
import pe.com.claro.sh.programainformativa.factura.bean.notificaplantilla.NotificarPlantillaResponse;
import pe.com.claro.sh.programainformativa.factura.exception.WSException;
import pe.com.claro.sh.programainformativa.factura.util.Constantes;
import pe.com.claro.sh.programainformativa.factura.util.PropertiesExternos;

@Service
public class NotificacionInformativaImpl implements NotificacionInformativa {

	private static Logger logger = Logger.getLogger(NotificacionInformativaImpl.class);

	@Autowired
	private PropertiesExternos propertiesexternos;

	@Override
	public NotificarPlantillaResponse notificarPlantilla( String mensajeTransaccion,String fecha, String  idTransaccion
			,String  userId,
			NotificarPlantillaRequest request) throws WSException {

		String mensajeMetodo = mensajeTransaccion + "[notificarPlantilla] ";

		NotificarPlantillaResponse notificarPlantillaResponse = new NotificarPlantillaResponse();

		String url = propertiesexternos.wsNotificaInformativaEndPoint;
		Gson inputGson = new Gson();

		try {


			String inputJson = inputGson.toJson(request, NotificarPlantillaRequest.class);
			
			logger.info(mensajeMetodo + "JSON: " + inputJson );

			Client client = Client.create();

			client.setConnectTimeout(propertiesexternos.wsNotificaInformativaConnectionTimeout);
			client.setReadTimeout(propertiesexternos.wsNotificaInformativaTimeout);

			WebResource webResource = client.resource(url);

			ClientResponse response = null;

			response = webResource.accept(MediaType.APPLICATION_JSON)
						.header(Constantes.NAMEHEADERAPPCONTENTTYPE,MediaType.APPLICATION_JSON)
			            .header(Constantes.NAMEHEADERAPPTIMESTAMP, fecha)
			            .header( Constantes.NAMEHEADERAPPIDTRANSACCION, idTransaccion)
			            .header(Constantes.NAMEHEADERAPPUSER, userId)
			            .type( MediaType.APPLICATION_JSON)
						.post(ClientResponse.class, inputJson);
			
			if(response != null){
				logger.info( mensajeMetodo + "Status: " + response.getStatus());
				
				String rpta = response.getEntity(String.class);
				
				logger.info( mensajeMetodo + "Parametros Salida:" + rpta);
				
				if(response.getStatus() == 200){
					notificarPlantillaResponse = new NotificarPlantillaResponse();
					Gson gson = new Gson();
					notificarPlantillaResponse = gson.fromJson(rpta, NotificarPlantillaResponse.class);	
					
				}else{
					logger.info( mensajeMetodo + "Error en el Servicio WS NotificacionInformativa Error:" +   response.getStatus());
			
				}	
			}
			

		} catch (Exception e) {
			logger.error( mensajeMetodo + "Excepcion ocurrida en el WS [" + "WS NotificacionInformativa"+ "]: [" + e.getMessage() + "]", e );
		} finally{
			logger.info( mensajeMetodo + "[FIN] - METODO: [NotificarPlantilla - WS] ");
		}
		

		return notificarPlantillaResponse;
	}

}
