package pe.com.claro.sh.programainformativa.factura.dao;

import pe.com.claro.sh.programainformativa.factura.bean.ObtieneCampanaActivaRequest;
import pe.com.claro.sh.programainformativa.factura.bean.ObtieneCampanaActivaResponse;
import pe.com.claro.sh.programainformativa.factura.exception.DBException;

public interface NotifDao {

	ObtieneCampanaActivaResponse buscarCampanaActiva(String msjTransaccion, ObtieneCampanaActivaRequest request)throws DBException;
}
