package pe.com.claro.sh.programainformativa.factura.util;

public class Constantes{
    
    
	public static final String LOG4J_PROPERTIES                             = "log4j.properties";
    public static final String CONFIG_PATH                                  = "./spring/applicationContext.xml";
    public static final String VACIO                                        = "";
    public static final String ESPACIO                                      = " ";
    public static final String COMILLA                                      = "'";
    public static final char   COMILLA_DOBLE                                = '"';
    public static final String STRING_PUNTO                                 = ".";
    public static final String SALTO_LINEA                                  = "\n";
    public static final String SLASH                                        = "/";
    public static final String SEPARADOR                                    = "\\|";
    public static final String ARROBA                                       = "@";
    public static final String PUNTO_COMA                                   = ";";
    public static final String PALOTE                                       = "|";
    public static final String SALTO_LINEASP								="\r\n";
    public static final String GUION_ABAJO                                  = "_";
    public static final String EJECUCION_SECUENCIA                          = "&&";
    public static final String TIEMPO_EJECUCION                          	= "Tiempo que demoro en la ejecucion[";	
    public static final String CORCHETE_INICIO                          	= "[";	
    public static final String CORCHETE_FIN                          		= "]";	
    
    // FORMATO PESO
    public static final String TAMANO_MEGA                                  = "[MB]";
    public static final short  MEGA                                         = 1024;
    public static final short  BUFFER                                       = 8;
    public static final String FORMATO_DECIMAL                              = "#,##";
    public static final String FORMATO_RANDON                               = "%04d";
    public static final String REGISTROS                             	    = " registros";
    
    // FORMATO FECHA OAC
    public static final String FECHA_OAC                                    = "dd/MM/yyyy";
    
    // VALOR RANDON
    public static final int    VALOR_RANDON                                 = 10000;
    public static final int    VALOR_RANDON_DEC                                 = 11;
    public static final String CON_CARGO= "Si";
    
    // COMANDO UNXI
    public static final String COMANDO_SH                                   = "sh";
    public static final String COMANDO_C                                    = "-c";
    public static final String COMANDO_CD                                   = "cd";
    public static final String COMANDO_AWK                                  = "awk ";
    public static final String COMANDO_AWK_LINEAS                           = "awk 'NR%";
    public static final String COMANDO_AWK_BEGIN                            = " BEGIN{FS= ";
    public static final String COMANDO_MOSTRAR                              = "echo";
    public static final String COMND_CONTAR_LINEAS                          = "wc -l";
    
    // WS REST
    public static final String VALUEAPPLICATIONJSON                         = "application/json";
    
    // WS REST HEADER
    public static final String NAMEHEADERAPPCONTENTTYPE = "content-type";
    public static final String NAMEHEADERAPPUSER                            = "userId";
    public static final String NAMEHEADERAPPIDTRANSACCION                   = "idTransaccion";
    public static final String NAMEHEADERAPPTIMESTAMP                       = "timestamp";
    
    // VARIABLES CTL
    public static final String TABLA_TEMPORAL                               = "OHXACT_NOTIF_INFOR";
    public static final String TABLA_FECH_VENC                               = "OHXACTVENC_NOTIF_INFOR";
    public static final String SIGNO_REPLACE_COMA                           = ",";
    public static final String ESPACIO_BLANCO                               = " ";
    public static final String CTL_SQLLDR                                   = "sqlldr ";
    public static final String CTL_USER_ID                                  = "userid=";
    public static final String CTL_CONTROL                                  = "control=";
    public static final String CTL_DATA                                     = "data=";
    public static final String CTL_LOG                                      = "log=";
    public static final String CTL_BAD                                      = "bad=";
    public static final String CTL_DISCARD                                  = "discard=";
    public static final String CTL_BINDSIZE                                 = "bindsize=";
    public static final String CTL_READSIZE                                 = "readsize=";
    public static final String CTL_ROWS                                     = "rows=";
    public static final String CTL_ERROS                                    = "errors=";
    
    // SFTP
    public static final String NO_MINUSCULA                                 = "no";
    
    // IDF
    public static final String ERROR_DETALLE                                = "Detalle Error:";
    public static final String CODIGO_ERROR                               	= "[CODERROR]:";
    public static final String MENSAJE_ERROR                               	= "[MSGERROR]: ";
    public static final String CODIGO_EXITO_0                               = "0";
    public static final String CODIGO_EXITO_2                               = "2";
    
    // IDT
    public static final String CODIGO_ERROR_1                               = "-1";
    public static final int    INT_UNO                                      = 1;
    public static final int    INT_CERO                                     = 0;
    public static final int    INT_DOS                                      = 2;
    public static final int    INT_TRES                                     = 3;
    public static final String CODIGO_ERROR_TIMEOUT                         = "-2";
    public static final String CODIGO_ERROR_GENERICO                        = "-3";
    public static final String FORMATO_FECHA                                = "yyyy-MM-dd";
    public static final String FORMATO_FECHA_REST                           = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    
    // ERRORES BASE DE DATOS
    public static final String ORA_01013                                    = "ORA-01013: user requested cancel of current operation";
    public static final String TEXTO_COULD_NOT_GET_CONNECTION               = "Could not get Connection";
    public static final String ERROR_NO_CONEXION                            = "-222";
    public static final String ERROR_TIMEOUT                                = "-111";
    public static final String ERROR_GENERAL                                = "-333";
    
    // Mensajes DAO
    public static final String METODO_DAO_MENSAJE_CONSULTANDO_BD            = "Consultando BD ";
    public static final String METODO_DAO_MENSAJE_CON_JNDI                  = ", con JNDI = [";
    public static final String METODO_DAO_MENSAJE_SE_INVOCARA_SP            = "Se invocara el SP: ";
    public static final String METODO_DAO_MENSAJE_PARAMETROS_INPUT          = "PARAMETROS [INPUT]: ";
    public static final String METODO_DAO_MENSAJE_SE_INVOCO_CON_EXITO       = "Se invoco con exito el SP: ";
    public static final String METODO_DAO_MENSAJE_PARAMETROS_OUTPUT         = "PARAMETROS [OUPUT]: ";
    public static final String METODO_DAO_MENSAJE_NESTEDRUNTIMEEXCEPTION    = "Se ha producido un error [NestedRuntimeException] definido como:\n";
    public static final String METODO_DAO_MENSAJE_EXCEPTION                 = "Se ha producido un error [Exception] definido como:\n";
    public static final String SP_MENSAJE_ERROR_EJECUTANDO                  = "Error ejecutando";
    public static final String BD_CON_CORCHETE                              = "[$bd]";
    public static final String WS_CON_CORCHETE                              = "[$ws]";
    public static final String MENSAJE_EXITO                                = "Operacion exitosa";
    public static final String MENSAJE_ERROR_ENVIO_CORREO                   = "No se pudo enviar correo de notificacion: Error en el servicio [$ws]";
    public static final String VALOR_ID                                     = "[VALOR_ID]";
    public static final String VALOR_CODCUENTA                              = "[VALOR_CODCUENTA]";
    
    // SP EAISS_BUSCAR_CAMP_ACTIVO NOTIFDB NOTIFDB
    public static final String SP_NOTIFDB_BUSQ_PI_K_PLANTILLA               = "K_PLANTILLA";
    public static final String SP_NOTIFDB_BUSQ_PO_CUR_CAMP_ACTIVO           = "K_NOTIFICACIONES";
    public static final String SP_NOTIFDB_BUSQ_PO_MSJ_RESULTADO             = "K_MSJ_RESULTADO";
    public static final String SP_NOTIFDB_BUSQ_PO_COD_RESULTADO             = "K_COD_RESULTADO";
    
    // Cursor SP EAISS_BUSCAR_CAMP_ACTIVO
    public static final String CURSOR_SP_EAISS_BUSCAR_CAMP_ACTIVO_CODTPL    = "CODTPL";
    public static final String CURSOR_SP_EAISS_BUSCAR_CAMP_ACTIVO_TIPO      = "TIPO";
    public static final String CURSOR_SP_EAISS_BUSCAR_CAMP_ACTIVO_CODCANAL  = "CODCANAL";
    
    // SP PR_LISTAR_EMISION_RECIBOS OAC
    public static final String SP_OAC_EMISION_RECIBO_LISTAR_EMISION_RECIBOS = "PR_LISTAR_EMISION_RECIBOS";
    public static final String SP_OAC_EMISION_RECIBO_PI_PD_FECHA_EMISION    = "PD_FECHA_EMISION";
    public static final String SP_OAC_EMISION_RECIBO_PD_FECHA_VENCIMIENTO   = "PD_FECHA_VENCIMIENTO";
    public static final String SP_OAC_EMISION_RECIBO_PD_FECHA_PROX_VENC     = "PD_FECHA_PROX_VENC";
    public static final String SP_OAC_EMISION_RECIBO_PO_CURSOR              = "P_CURSOR_NOTIF";
    public static final String SP_OAC_EMISION_RECIBO_PO_COD_RPTA            = "COD_RPTA";
    public static final String SP_OAC_EMISION_RECIBO_PO_MSG_RPTA            = "MSG_RPTA";
    
    // Cursor SP PR_LISTAR_EMISION_RECIBOS
    public static final String CURSOR_SP_PR_LISTAR_FACTURA                  = "OHXACT";
    public static final String CURSOR_SP_PR_LISTAR_ORIGEN                   = "ORIGEN";
    
    // Cursor SP FECHA_VENCIMIENTO
    public static final String CURSOR_SP_FECHA_VENCIMIENTO_FACTURA          = "FACTURA";
    public static final String CURSOR_SP_FECHA_VENCIMIENTO_ORIGEN           = "ORIGEN";
    public static final String CURSOR_SP_FECHA_VENCIMIENTO_FECHA_VENC       = "FECHA_VENCIMIENTO";
    
    // SP MSJSS_MONTOS_ADICIONALES BSCS
    public static final String SP_BSCS_MONTOS_ADICIONALES_PO_COD_RPTA       = "PO_COD_ERR";
    public static final String SP_BSCS_MONTOS_ADICIONALES_PO_MSG_RPTA       = "PO_DES_ERR";
    
    // SP MSJSS_FECHA_VENCIMIENTO BSCS
    public static final String SP_BSCS_FECHA_VENCIMIENTO_PI_FECH       		= "PI_FECHA_PROX_VENC";
}
