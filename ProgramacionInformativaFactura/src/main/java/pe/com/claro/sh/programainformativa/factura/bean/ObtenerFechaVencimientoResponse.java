package pe.com.claro.sh.programainformativa.factura.bean;

import java.util.ArrayList;
import java.util.List;

public class ObtenerFechaVencimientoResponse  extends RespuestaBean {

	private static final long serialVersionUID =1L;
	private List<CursorFechaVencimiento> listFechaVencimiento = new ArrayList<>();
	
	public List<CursorFechaVencimiento> getListFechaVencimiento() {
		return listFechaVencimiento;
	}
	public void setListFechaVencimiento(List<CursorFechaVencimiento> listFechaVencimiento) {
		this.listFechaVencimiento = listFechaVencimiento;
	}
	
	
	
	
	
}
