package pe.com.claro.sh.programainformativa.factura.bean;

import java.util.List;

public class ObtieneCampanaActivaResponse  extends RespuestaBean{
	
	private List<CursorCampanaNotifcar> cursorCampanaNotifca;

	public List<CursorCampanaNotifcar> getCursorCampanaNotifca() {
		return cursorCampanaNotifca;
	}

	public void setCursorCampanaNotifca(List<CursorCampanaNotifcar> cursorCampanaNotifca) {
		this.cursorCampanaNotifca = cursorCampanaNotifca;
	}
	
	

}
