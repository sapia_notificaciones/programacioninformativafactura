package pe.com.claro.sh.programainformativa.factura.dao;

import pe.com.claro.sh.programainformativa.factura.bean.ObtenerFacturasEmitidasRequest;
import pe.com.claro.sh.programainformativa.factura.bean.ObtenerFacturasEmitidasResponse;
import pe.com.claro.sh.programainformativa.factura.bean.ObtenerFechaVencimientoRequest;
import pe.com.claro.sh.programainformativa.factura.bean.ObtenerFechaVencimientoResponse;
import pe.com.claro.sh.programainformativa.factura.exception.DBException;

public interface OacDao {

	ObtenerFacturasEmitidasResponse listarFacturasEmitidas(String msjTransaccion,
			ObtenerFacturasEmitidasRequest request) throws DBException;

	ObtenerFechaVencimientoResponse listarFechaVencimiento(String msjTransaccion,
			ObtenerFechaVencimientoRequest request) throws DBException;
}
