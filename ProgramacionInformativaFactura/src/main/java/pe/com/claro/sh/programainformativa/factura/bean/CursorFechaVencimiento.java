package pe.com.claro.sh.programainformativa.factura.bean;

import java.io.Serializable;

public class CursorFechaVencimiento implements  Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String factura;
	private String origen;
	
	
	public String getFactura() {
		return factura;
	}
	public void setFactura(String factura) {
		this.factura = factura;
	}
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	
	
	
	
	
	
	
}
