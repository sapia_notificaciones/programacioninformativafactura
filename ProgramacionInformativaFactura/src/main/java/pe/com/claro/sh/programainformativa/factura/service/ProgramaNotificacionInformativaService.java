package pe.com.claro.sh.programainformativa.factura.service;

public interface ProgramaNotificacionInformativaService {
	
	void procesar(String msTxIn,String idTransaccion , String idPlantilla);

}
