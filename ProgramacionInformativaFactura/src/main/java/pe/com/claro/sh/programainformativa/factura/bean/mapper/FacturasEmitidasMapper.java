package pe.com.claro.sh.programainformativa.factura.bean.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.sh.programainformativa.factura.bean.CursorFacturasEmitidas;
import pe.com.claro.sh.programainformativa.factura.util.Constantes;

public class FacturasEmitidasMapper  implements RowMapper<CursorFacturasEmitidas>{

	@Override
	public CursorFacturasEmitidas mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		CursorFacturasEmitidas listaFacturas=new CursorFacturasEmitidas();
		listaFacturas.setFactura(rs.getString(Constantes.CURSOR_SP_PR_LISTAR_FACTURA));
		listaFacturas.setOrigenFactura(rs.getString(Constantes.CURSOR_SP_PR_LISTAR_ORIGEN));
		
		return listaFacturas;
	}

}
