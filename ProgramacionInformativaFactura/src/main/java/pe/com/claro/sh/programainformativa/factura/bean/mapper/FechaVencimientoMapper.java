package pe.com.claro.sh.programainformativa.factura.bean.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.sh.programainformativa.factura.bean.CursorFechaVencimiento;
import pe.com.claro.sh.programainformativa.factura.util.Constantes;
import pe.com.claro.sh.programainformativa.factura.util.Utilitarios;

public class FechaVencimientoMapper  implements RowMapper<CursorFechaVencimiento>{

	@Override
	public CursorFechaVencimiento mapRow(ResultSet rs, int rowNum) throws SQLException {

		CursorFechaVencimiento listaFechaVencimiento=new CursorFechaVencimiento();
		listaFechaVencimiento.setFactura(rs.getString(Constantes.CURSOR_SP_FECHA_VENCIMIENTO_FACTURA));
		listaFechaVencimiento.setOrigen(rs.getString(Constantes.CURSOR_SP_FECHA_VENCIMIENTO_ORIGEN));
		return listaFechaVencimiento;
	}
	
	

}
