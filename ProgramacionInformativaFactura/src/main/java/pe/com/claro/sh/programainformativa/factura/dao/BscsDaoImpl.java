package pe.com.claro.sh.programainformativa.factura.dao;

import java.math.BigDecimal;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.com.claro.sh.programainformativa.factura.bean.RespuestaBean;
import pe.com.claro.sh.programainformativa.factura.exception.DBException;
import pe.com.claro.sh.programainformativa.factura.util.Constantes;
import pe.com.claro.sh.programainformativa.factura.util.PropertiesExternos;
import pe.com.claro.sh.programainformativa.factura.util.Utilitarios;

@Repository
public class BscsDaoImpl implements BscsDao {

	private static transient Logger logger = Logger.getLogger(BscsDaoImpl.class);
	@Autowired
	private PropertiesExternos propertiesExterno;
	@Autowired
	@Qualifier("bscsdbDS")
	private DataSource bscsdbDS;

	@Override
	public RespuestaBean consultarMontosAdicionales(String msjTransaccion) throws DBException {

		long tiempoInicio = Utilitarios.obtenerTiempoEnMiliSegundos();

		String msjTx = msjTransaccion + "[consultarMontosAdicionales]";
		logger.info(msjTx + "Inicio del metodo consultarMontosAdicionales ");
		String owner = propertiesExterno.dbBscsdbOwner;
		String pkg = propertiesExterno.dbBscsdbPkgOacNotifInfo;
		String sp = propertiesExterno.dbBscsdbSpEaisCargosAdicionales;
		RespuestaBean consultarMontosAdicionales = new RespuestaBean();
		try {

			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(bscsdbDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(owner).withCatalogName(pkg).withProcedureName(sp).declareParameters(
							new SqlOutParameter(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_COD_RPTA, OracleTypes.NUMBER),
							new SqlOutParameter(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_MSG_RPTA,
									OracleTypes.VARCHAR));
			logger.info(msjTx + "[BD]" + propertiesExterno.dbBscsdb + Constantes.METODO_DAO_MENSAJE_SE_INVOCARA_SP
					+ propertiesExterno.dbBscsdbOwner + Constantes.STRING_PUNTO
					+ propertiesExterno.dbBscsdbPkgOacNotifInfo + Constantes.STRING_PUNTO
					+ propertiesExterno.dbBscsdbSpEaisCargosAdicionales);
			jdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.dbBscsdbTimeOutExecutionMaxTime);
			Map<String, Object> resultMap = jdbcCall.execute();
			if (resultMap != null && (!resultMap.isEmpty())) {

				consultarMontosAdicionales
						.setCodRespuesta((BigDecimal) resultMap.get(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_COD_RPTA));
				consultarMontosAdicionales.setMsjRespuesta(
						(String) resultMap.get(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_MSG_RPTA).toString());
				logger.info(msjTx + "PARAMETROS [OUTPUT]:");
				logger.info(msjTx + "[PO_COD_ERR]: " + consultarMontosAdicionales.getCodRespuesta());
				logger.info(msjTx + "[PO_DES_ERR]: " + consultarMontosAdicionales.getMsjRespuesta());
			} else {
				logger.info(msjTx + "No se encontraron datos para procesar.");
			}
		} catch (Exception e) {

			imprimirLogBD(msjTx, e);
		} finally {
			logger.info(msjTx + "Tiempo Transacurrido (ms): [" + (System.currentTimeMillis() - tiempoInicio) + "]");
			logger.info(msjTx + "[FIN] - METODO: [RegistrarOrigen - DAO] ");
		}
		return consultarMontosAdicionales;
	}

	private void imprimirLogBD(String msjTx, Exception e) throws DBException {

		validaExceptionBD(msjTx, e);
	}

	private void validaExceptionBD(String msjTx, Exception e) throws DBException {
		String codigoError;
		if (String.valueOf(e.getMessage()).contains(Constantes.TEXTO_COULD_NOT_GET_CONNECTION)) {

			logger.error(msjTx + "Error de conexion a BD: " + propertiesExterno.dbBscsdb, e);
			codigoError = Constantes.ERROR_NO_CONEXION;
		} else if (String.valueOf(e.getMessage()).contains(Constantes.ORA_01013)) {
			logger.error(msjTx + "Error de Timeout en BD: " + propertiesExterno.dbBscsdb, e);
			codigoError = Constantes.ERROR_TIMEOUT;
		} else {
			logger.error(msjTx + "Error general en BD: " + propertiesExterno.dbBscsdb, e);
			codigoError = Constantes.ERROR_GENERAL;
		}
		throw new DBException(codigoError, e.getMessage(), propertiesExterno.dbBscsdb,
				propertiesExterno.dbBscsdbOwner + "." + propertiesExterno.dbBscsdbPkgOacNotifInfo + "."
						+ propertiesExterno.dbBscsdbSpEaisCargosAdicionales,
				e);
	}

	@Override
	public RespuestaBean depurarEmsionRecibo(String mensaje) throws DBException {

		long tiempoInicio = System.currentTimeMillis();

		String msjTx = mensaje + "[Borrar Tabla Temporal] ";
		logger.info(msjTx + "[INICIO] - METODO: [DepuraTablaEmision - DAO] ");
		RespuestaBean response = new RespuestaBean();
		String nombreBD = propertiesExterno.dbBscsdb;
		String owner = propertiesExterno.dbBscsdbOwner;
		String paquete = propertiesExterno.dbBscsdbPkgOacNotifInfo;
		String procedimiento = propertiesExterno.dbBscsdbSpBorrarTabla;
		logger.info(msjTx + "Consultando a la BD: " + nombreBD);
		logger.info(msjTx + "Ejecutando PROCEDURE: [" + owner + Constantes.STRING_PUNTO + paquete
				+ Constantes.STRING_PUNTO + procedimiento + "]");
		try {

			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(bscsdbDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(owner).withCatalogName(paquete).withProcedureName(procedimiento).declareParameters(
							new SqlOutParameter(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_COD_RPTA, OracleTypes.NUMBER),
							new SqlOutParameter(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_MSG_RPTA,
									OracleTypes.VARCHAR));
			jdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.dbBscsdbTimeOutExecutionMaxTime);
			logger.info(msjTx + Constantes.METODO_DAO_MENSAJE_SE_INVOCARA_SP + owner + Constantes.STRING_PUNTO + paquete
					+ Constantes.STRING_PUNTO + procedimiento);
			MapSqlParameterSource map = new MapSqlParameterSource();
			Map<String, Object> result = jdbcCall.execute(map);
			if (!(result == null || result.isEmpty())) {
				response.setCodRespuesta((BigDecimal) result.get(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_COD_RPTA));
				response.setMsjRespuesta(
						(String) result.get(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_MSG_RPTA).toString());
				logger.info(msjTx + "PARAMETROS [OUTPUT]:");
				logger.info(msjTx + "[PO_COD_ERR]: " + response.getCodRespuesta());
				logger.info(msjTx + "[PO_DES_ERR]: " + response.getMsjRespuesta());
			} else {

				logger.info(msjTx + "No se encontraron datos para procesar.");
			}
		} catch (Exception e) {
			imprimirLogBD(msjTx, e);
		} finally {
			logger.info(msjTx + "Tiempo Transacurrido (ms): [" + (System.currentTimeMillis() - tiempoInicio) + "]");
			logger.info(msjTx + "[FIN] - METODO: [RegistrarOrigen - DAO] ");
		}
		return response;
	}

	public RespuestaBean consultarFechaVencimiento(String msjTransaccion, String fechaVencimiento) throws DBException {

		long tiempoInicio = Utilitarios.obtenerTiempoEnMiliSegundos();

		String msjTx = msjTransaccion + "[consultarFechaVencimiento]";
		logger.info(msjTx + "Inicio del metodo consultarFechaVencimiento ");
		String owner = propertiesExterno.dbBscsdbOwner;
		String pkg = propertiesExterno.dbBscsdbPkgOacNotifInfo;
		String sp = propertiesExterno.dbBscsdbSpFechVenc;
		RespuestaBean consultarMontosAdicionales = new RespuestaBean();
		try {

			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(bscsdbDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(owner).withCatalogName(pkg).withProcedureName(sp).declareParameters(
							new SqlParameter(Constantes.SP_BSCS_FECHA_VENCIMIENTO_PI_FECH, OracleTypes.VARCHAR),
							new SqlOutParameter(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_COD_RPTA, OracleTypes.NUMBER),
							new SqlOutParameter(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_MSG_RPTA,OracleTypes.VARCHAR));
			logger.info(msjTx + "[BD]" + propertiesExterno.dbBscsdb + Constantes.METODO_DAO_MENSAJE_SE_INVOCARA_SP
					+ propertiesExterno.dbBscsdbOwner + Constantes.STRING_PUNTO
					+ propertiesExterno.dbBscsdbPkgOacNotifInfo + Constantes.STRING_PUNTO
					+ propertiesExterno.dbBscsdbSpFechVenc);
					
			
			logger.info(msjTx + "PARAMETROS [INPUT]: ");
			logger.info(msjTx + "[PD_FECHA_EMISION] = " + fechaVencimiento);
			
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
			.addValue(Constantes.SP_BSCS_FECHA_VENCIMIENTO_PI_FECH,fechaVencimiento);
			
			jdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.dbBscsdbTimeOutExecutionMaxTime);
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);
			if (resultMap != null && (!resultMap.isEmpty())) {

				consultarMontosAdicionales
						.setCodRespuesta((BigDecimal) resultMap.get(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_COD_RPTA));
				consultarMontosAdicionales.setMsjRespuesta(
						(String) resultMap.get(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_MSG_RPTA).toString());
				logger.info(msjTx + "PARAMETROS [OUTPUT]:");
				logger.info(msjTx + "[PO_COD_ERR]: " + consultarMontosAdicionales.getCodRespuesta());
				logger.info(msjTx + "[PO_DES_ERR]: " + consultarMontosAdicionales.getMsjRespuesta());
			} else {
				logger.info(msjTx + "No se encontraron datos para procesar.");
			}
		} catch (Exception e) {

			imprimirLogBD(msjTx, e);
		} finally {
			logger.info(msjTx + "Tiempo Transacurrido (ms): [" + (System.currentTimeMillis() - tiempoInicio) + "]");
			logger.info(msjTx + "[FIN] - METODO: [RegistrarOrigen - DAO] ");
		}
		return consultarMontosAdicionales;
	}
	
	public RespuestaBean consultarFechaProxVencimiento(String msjTransaccion, String fechaVencimiento) throws DBException {

		long tiempoInicio = Utilitarios.obtenerTiempoEnMiliSegundos();

		String msjTx = msjTransaccion + "[consultarFechaProxVencimiento]";
		logger.info(msjTx + "Inicio del metodo consultarFechaProxVencimiento ");
		String owner = propertiesExterno.dbBscsdbOwner;
		String pkg = propertiesExterno.dbBscsdbPkgOacNotifInfo;
		String sp = propertiesExterno.dbBscsdbSpFechVenc;
		RespuestaBean consultarFechaProxima = new RespuestaBean();
		try {

			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(bscsdbDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(owner).withCatalogName(pkg).withProcedureName(sp).declareParameters(
							new SqlParameter(Constantes.SP_BSCS_FECHA_VENCIMIENTO_PI_FECH, OracleTypes.VARCHAR),
							new SqlOutParameter(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_COD_RPTA, OracleTypes.NUMBER),
							new SqlOutParameter(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_MSG_RPTA,OracleTypes.VARCHAR));
			
			logger.info(msjTx + "[BD]" + propertiesExterno.dbBscsdb + Constantes.METODO_DAO_MENSAJE_SE_INVOCARA_SP
					+ propertiesExterno.dbBscsdbOwner + Constantes.STRING_PUNTO
					+ propertiesExterno.dbBscsdbPkgOacNotifInfo + Constantes.STRING_PUNTO
					+ propertiesExterno.dbBscsdbSpFechVenc);
			
			logger.info(msjTx + "PARAMETROS [INPUT]: ");
			logger.info(msjTx + Constantes.CORCHETE_INICIO+Constantes.SP_BSCS_FECHA_VENCIMIENTO_PI_FECH+Constantes.CORCHETE_INICIO+ fechaVencimiento);
			
                        SqlParameterSource objParametrosIN = new MapSqlParameterSource()
                        .addValue(Constantes.SP_BSCS_FECHA_VENCIMIENTO_PI_FECH,fechaVencimiento);

			jdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.dbBscsdbTimeOutExecutionMaxTime);
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);
			if (resultMap != null && (!resultMap.isEmpty())) {

				consultarFechaProxima
						.setCodRespuesta((BigDecimal) resultMap.get(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_COD_RPTA));
				consultarFechaProxima.setMsjRespuesta(
						(String) resultMap.get(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_MSG_RPTA).toString());
				logger.info(msjTx + "PARAMETROS [OUTPUT]:");
				logger.info(msjTx + "[PO_COD_ERR]: " + consultarFechaProxima.getCodRespuesta());
				logger.info(msjTx + "[PO_DES_ERR]: " + consultarFechaProxima.getMsjRespuesta());
			} else {
				logger.info(msjTx + "No se encontraron datos para procesar.");
			}
		} catch (Exception e) {

			imprimirLogBD(msjTx, e);
		} finally {
			logger.info(msjTx + "Tiempo Transacurrido (ms): [" + (System.currentTimeMillis() - tiempoInicio) + "]");
			logger.info(msjTx + "[FIN] - METODO: [RegistrarOrigen - DAO] ");
		}
		return consultarFechaProxima;
	}

	@Override
	public RespuestaBean eliminarFactVencidas(String mensaje) throws DBException {

		long tiempoInicio = System.currentTimeMillis();

		String msjTx = mensaje + "[Borrar Tabla Temporal] ";
		logger.info(msjTx + "[INICIO] - METODO: [EliminarUsuarioAppTemporal - DAO] ");
		RespuestaBean response = new RespuestaBean();
		String nombreBD = propertiesExterno.dbBscsdb;
		String owner = propertiesExterno.dbBscsdbOwner;
		String paquete = propertiesExterno.dbBscsdbPkgOacNotifInfo;
		String procedimiento = propertiesExterno.dbBscsdbSpDeletFacVencidas;
		logger.info(msjTx + "Consultando a la BD: " + nombreBD);
		logger.info(msjTx + "Ejecutando PROCEDURE: [" + owner + Constantes.STRING_PUNTO + paquete
				+ Constantes.STRING_PUNTO + procedimiento + "]");
		try {

			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(bscsdbDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(owner).withCatalogName(paquete).withProcedureName(procedimiento).declareParameters(
							new SqlOutParameter(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_COD_RPTA, OracleTypes.NUMBER),
							new SqlOutParameter(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_MSG_RPTA,
									OracleTypes.VARCHAR));
			jdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.dbBscsdbTimeOutExecutionMaxTime);
			logger.info(msjTx + Constantes.METODO_DAO_MENSAJE_SE_INVOCARA_SP + owner + Constantes.STRING_PUNTO + paquete
					+ Constantes.STRING_PUNTO + procedimiento);
			MapSqlParameterSource map = new MapSqlParameterSource();
			Map<String, Object> result = jdbcCall.execute(map);
			if (!(result == null || result.isEmpty())) {
				response.setCodRespuesta((BigDecimal) result.get(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_COD_RPTA));
				response.setMsjRespuesta(
						(String) result.get(Constantes.SP_BSCS_MONTOS_ADICIONALES_PO_MSG_RPTA).toString());
				logger.info(msjTx + "PARAMETROS [OUTPUT]:");
				logger.info(msjTx + "[PO_COD_ERR]: " + response.getCodRespuesta());
				logger.info(msjTx + "[PO_DES_ERR]: " + response.getMsjRespuesta());
			} else {

				logger.info(msjTx + "No se encontraron datos para procesar.");
			}
		} catch (Exception e) {
			imprimirLogBD(msjTx, e);
		} finally {
			logger.info(msjTx + "Tiempo Transacurrido (ms): [" + (System.currentTimeMillis() - tiempoInicio) + "]");
			logger.info(msjTx + "[FIN] - METODO: [RegistrarOrigen - DAO] ");
		}
		return response;
	}

}
