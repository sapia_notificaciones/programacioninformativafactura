package pe.com.claro.sh.programainformativa.factura.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesExternos {
    
	@Value("${log4j.file.dir}")
	public String vLog4JDir;

	// CTL
		@Value("${ctl.formato.fecha}")
		public String pFORMATO_FECHA_NOMBRE;

		@Value("${ctl.facturas.usuario}")
		public String userBSCSCTL;

		@Value("${ctl.facturas.pass}")
		public String passBSCSCTL;

		@Value("${ctl.facturas.conexion}")
		public String dbBSCSCTL;

		@Value("${ctl.facturas.file.dir}")
		public String vRutaCtlUserApp;

		@Value("${nombre.archivo.facturas.ctl}")
		public String pNOMBRE_ARCHIVO_FACTURA_CTL;

		@Value("${nombre.archivo.factVenc.ctl}")
		public String pNOMBRE_ARCHIVO_FACFECVEN_CTL;

		@Value("${extension.archivo.ctl}")
		public String pEXTENSION_ARCHIVO_CTL;

		@Value("${extension.archivo.bad}")
		public String pEXTENSION_ARCHIVO_BAD;

		@Value("${extension.archivo.dsc}")
		public String pEXTENSION_ARCHIVO_DSC;

		@Value("${extension.archivo.log}")
		public String pEXTENSION_ARCHIVO_LOG;
		
		@Value("${max.bindsize.ctl}")
		public int pBindSizeCTL;

		@Value("${max.readsize.ctl}")
		public int pReadSizeCTL;

		@Value("${max.rows.ctl}")
		public int prowsCTL;
		
		@Value("${max.errors.ctl}")
		public int perrorsCTL;
		
		// SQLLDR
		@Value("${log.ctl.facturas.file.dir}")
		public String vRutaLogCtlFactura;

		@Value("${data.ctl.facturas.file.dir}")
		public String vRutaDataCtlFactura;

		@Value("${bad.ctl.facturas.file.dir}")
		public String vRutaBadCtlFactura;

		@Value("${discard.ctl.facturas.file.dir}")
		public String vRutaDiscarCtlFactura;
	
	
	// Notificacion Informativa
	
	@Value("${ws.notificainformativa.plantilla}")
	public String wsNotificaInformativaPlantilla;

	@Value("${ws.notificainformativa.nombre}")
	public String wsNotificaInformativaNombre;

	@Value("${ws.notificainformativa.operacion}")
	public String wsNotificaInformativaOperacion;

	@Value("${ws.notificainformativa.endpoint}")
	public String wsNotificaInformativaEndPoint;

	@Value("${ws.notificainformativa.userId}")
	public String wsNotificaInformativaUserId;

	@Value("${ws.notificainformativa.ubicacion}")
	public String wsNotificaInformativaUbicacion;

	@Value("${ws.notificainformativa.request.timeout}")
	public Integer wsNotificaInformativaTimeout;

	@Value("${ws.notificainformativa.connection.timeout}")
	public Integer wsNotificaInformativaConnectionTimeout;
	
	//CANAL DE CONEXION SFTP
	@Value("${conexion.sftp.canal}")
    public String pTIPO_CANAL_SESION;
	
	//CONEXION FILESERVER  SFTP
	@Value("${sftp.fileserver.host}")
    public String FileServerIP;

	@Value("${sftp.fileserver.ruta}")
	public String FileServerRutaRemota;

	@Value("${sftp.fileserver.canal}")
	public String FileServerCanal;

	@Value("${sftp.fileserver.puerto}")
	public int FileServerPuerto;

	@Value("${sftp.fileserver.usuario}")
	public String FileServerUsuario;

	@Value("${sftp.fileserver.password}")
	public String FileServerPass;

	@Value("${sftp.fileserver.timeout}")
	public String FileServertimeOut;
	
	//CONEXION BSCS  SFTP
	
	@Value("${tamano.maximo.bscs}")
	public long TAMANO_MAXIMO_ARCHIVO;

	@Value("${cantidad.lineas.bscs}")
	public long CANTIDAD_LINEAS;

	@Value("${conexion.sftp.bscs.ip}")
	public String CONEXION_SFTP_BSCS_IP;

	@Value("${conexion.sftp.bscs.usuario}")
	public String CONEXION_SFTP_BSCS_USUARIO;
	
	@Value("${conexion.sftp.bscs.contrasena}")
	public String CONEXION_SFTP_BSCS_CONTRASENA;

	@Value("${conexion.sftp.bscs.ruta.archivo}")
	public String CONEXION_SFTP_BSCS_RUTA_ARCHIVO;

	@Value("${conexion.sftp.bscs.puerto}")
	public int CONEXION_SFTP_BSCS_PUERTO;



	@Value("${conexion.sftp.bscs.nombre.cargo.archivo}")
	public String CONEXION_SFTP_BSCS_NOMBRE_ARCHIVO_CARGO;
	
	@Value("${conexion.sftp.bscs.nombre.sincargo.archivo}")
	public String CONEXION_SFTP_BSCS_NOMBRE_ARCHIVO_SIN_CARGO;

	@Value("${conexion.sftp.bscs.base.archivo}")
	public String CONEXION_SFTP_BSCS_ARCHIVO_BASE;

	@Value("${conexion.sftp.bscs.fechVenc.archivo}")
	public String CONEXION_SFTP_BSCS_FECHVEN_BASE;

	@Value("${conexion.sftp.bscs.fechProxVenc.archivo}")
	public String CONEXION_SFTP_BSCS_FECHPROXVEN_BASE;

	@Value("${conexion.sftp.bscs.extension}")
	public String CONEXION_SFTP_BSCS_EXTENSION;

	@Value("${conexion.sftp.bscs.timeout}")
	public String CONEXION_SFTP_BSCS_TIMEOUT;

	
	// ARCHIVO EMISION DE RECIBO

	@Value("${sftp.extension.archivo.emision.recibo}")
	public String vEmisionReciboExtension;	
	
	@Value("${sftp.ruta.archivo.local.emision.recibo}")
	public String vRutaLocalEmisionRecibo;	

	@Value("${sftp.ruta.archivo.pendiente.envio.emision}")
	public String vRutaPendienteEnvioEmision;	

	@Value("${sftp.ruta.archivo.filtro.bscs}")
	public String vRutaLocalEmisionFiltro;	

	@Value("${sftp.ruta.archivo.posicion.factura}")
	public int vRutaLocalEmisionPosicion;	
	

	// BD -- NOTIFICA
	@Value("${db.notif.plantilla}")
	public String dbNotifdbPlantilla;

	
	// BD -- OAC

	@Value("${db.oac.oracle.jdbc.nombre}")
	public String dbOacdb;

	@Value("${db.oac.oracle.jdbc.owner}")
	public String dbOacdbOwner;

	@Value("${db.oac.oracle.jdbc.timeout.request}")
	public String dbOacdbTimeOutExecutionMaxTime;

	@Value("${db.oac.oracle.jdbc.notifica.package}")
	public String dbOacdbPkgOacNotifInfo;

	@Value("${db.oac.oracle.jdbc.factura.emitida.sp}")
	public String dbOacdbSpEaisFacturaEmitida;

	@Value("${db.oac.oracle.jdbc.lista.vencimiento.sp}")
	public String dbOacdbSpEaisListaVencimiento;
	
	
	@Value("${db.oac.formato.fecha.emision.recibo}")
	public String dbOacdbFormatoFecha;

	
	@Value("${emision.recibo.dias.procesar}")
	public int dbOacdbRangoFecha;

	@Value("${fecha.vencimiento.rango.dias}")
	public int dbOacdbRangoDiasVencmiento;

	@Value("${fecha.prox.vencimiento.adicion.dias}")
	public int dbOacdbRangoDiasAdicion;

	@Value("${fecha.prox.vencimiento.resta.dias}")
	public int dbOacdbRangoDiasResta;

	// BD -- BSCS
	@Value("${db.bscs.oracle.jdbc.nombre}")
	public String dbBscsdb;
	
	@Value("${db.bscs.oracle.jdbc.owner}")
	public String dbBscsdbOwner;

	
	@Value("${db.bscs.oracle.jdbc.timeout.request}")
	public int dbBscsdbTimeOutExecutionMaxTime;
	
	@Value("${db.bscs.oracle.jdbc.notifica.package}")
	public String dbBscsdbPkgOacNotifInfo;
	
	@Value("${db.bscs.oracle.jdbc.cargos.adicionales.sp}")
	public String dbBscsdbSpEaisCargosAdicionales;

	@Value("${db.bscs.oracle.jdbc.fech.venc.sp}")
	public String dbBscsdbSpFechVenc;

	@Value("${db.bscs.oracle.jdbc.limpiar.registros.sp}")
	public String dbBscsdbSpBorrarTabla;

	@Value("${db.bscs.oracle.jdbc.limpiar.facturas.sp}")
	public String dbBscsdbSpDeletFacVencidas;
	
	
	// CONFIGURACION ARCHIVO LOCAL 
	@Value("${local.nombre.archivo}")
	public String LOCAL_NOMBRE_ARCHIVO;
	
	@Value("${facVen.nombre.archivo}")
	public String FACVEN_NOMBRE_ARCHIVO;
	
	@Value("${local.extension.archivo}")
	public String LOCAL_EXTENSION_ARCHIVO;
	
	@Value("${local.ruta}")
	public String LOCAL_RUTA;
	
	// FICHERO NOTIFICACIONES INFORMATIVA
	@Value("${archivo.fecha.vencimiento}")
	public String vArchivoFechVenc;

	@Value("${archivo.base.fecha.vencimiento}")
	public String vArchivoBaseFechVenc;

	@Value("${archivo.proximo.fecha.vencimiento}")
	public String vArchivoProxFechVenc;

	
	// IDT FTP OAC
	@Value("${sftp.error.codigo}")
	public String pSFTP_ERROR_CODE;

	@Value("${sftp.error.detalle.sftp}")
	public String pSFTP_ERROR_DESCRIPTION_OBTENER;
	
	
	@Value("${tipo.separador}")
	public String pSeparador;
	
	// IDT DB
	@Value("${sp.idt1.codigo}")
	public String spIdt1Codigo;

	@Value("${sp.idt1.mensaje}")
	public String spIdt1Mensaje;

	@Value("${sp.idt2.codigo}")
	public String spIdt2Codigo;

	@Value("${sp.idt2.mensaje}")
	public String spIdt2Mensaje;

	
	// IDT EXCEPCIONES
	@Value( "${idt.1.codigo}" )
	public String				IDT1_CODIGO;
	@Value( "${idt.1.mensaje}" )
	public String				IDT1_MENSAJE;

}
