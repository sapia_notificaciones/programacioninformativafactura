package pe.com.claro.sh.programainformativa.factura.bean;

import java.io.Serializable;

public class CursorFacturasEmitidas  implements  Serializable{
	
	private static final long serialVersionUID = 1L;

	private String factura;
	private String origenFactura;
	
	
	
	public String getFactura() {
		return factura;
	}
	public void setFactura(String factura) {
		this.factura = factura;
	}
	public String getOrigenFactura() {
		return origenFactura;
	}
	public void setOrigenFactura(String origenFactura) {
		this.origenFactura = origenFactura;
	}
	
}
