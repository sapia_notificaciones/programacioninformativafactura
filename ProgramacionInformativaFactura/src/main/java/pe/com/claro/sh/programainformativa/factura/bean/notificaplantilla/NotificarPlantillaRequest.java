package pe.com.claro.sh.programainformativa.factura.bean.notificaplantilla;

public class NotificarPlantillaRequest{
    
    
  
    private String  plantilla;
    private String  ubicacionRepositorio;
    private Archivo archivo;
    
	public String getPlantilla() {
		return plantilla;
	}
	public void setPlantilla(String plantilla) {
		this.plantilla = plantilla;
	}
	public String getUbicacionRepositorio() {
		return ubicacionRepositorio;
	}
	public void setUbicacionRepositorio(String ubicacionRepositorio) {
		this.ubicacionRepositorio = ubicacionRepositorio;
	}
	public Archivo getArchivo() {
		return archivo;
	}
	public void setArchivo(Archivo archivo) {
		this.archivo = archivo;
	}
    
   
    
    
    
}
