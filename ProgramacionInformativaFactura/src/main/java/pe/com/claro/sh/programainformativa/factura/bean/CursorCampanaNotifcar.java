package pe.com.claro.sh.programainformativa.factura.bean;

public class CursorCampanaNotifcar {

	
	private String codPlantilla;
	private String tipoNotificacion;
	private String codCanal;
	
	
	public String getCodPlantilla() {
		return codPlantilla;
	}
	public void setCodPlantilla(String codPlantilla) {
		this.codPlantilla = codPlantilla;
	}
	public String getTipoNotificacion() {
		return tipoNotificacion;
	}
	public void setTipoNotificacion(String tipoNotificacion) {
		this.tipoNotificacion = tipoNotificacion;
	}
	public String getCodCanal() {
		return codCanal;
	}
	public void setCodCanal(String codCanal) {
		this.codCanal = codCanal;
	}
	@Override
	public String toString() {
		return "CursorCampanaNotifcar [codPlantilla=" + codPlantilla + ", tipoNotificacion=" + tipoNotificacion
				+ ", codCanal=" + codCanal + "]";
	}


	

}
