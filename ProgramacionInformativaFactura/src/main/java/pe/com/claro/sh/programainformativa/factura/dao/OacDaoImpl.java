package pe.com.claro.sh.programainformativa.factura.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.NestedRuntimeException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.com.claro.sh.programainformativa.factura.bean.CursorFacturasEmitidas;
import pe.com.claro.sh.programainformativa.factura.bean.CursorFechaVencimiento;
import pe.com.claro.sh.programainformativa.factura.bean.ObtenerFacturasEmitidasRequest;
import pe.com.claro.sh.programainformativa.factura.bean.ObtenerFacturasEmitidasResponse;
import pe.com.claro.sh.programainformativa.factura.bean.ObtenerFechaVencimientoRequest;
import pe.com.claro.sh.programainformativa.factura.bean.ObtenerFechaVencimientoResponse;
import pe.com.claro.sh.programainformativa.factura.bean.mapper.FacturasEmitidasMapper;
import pe.com.claro.sh.programainformativa.factura.bean.mapper.FechaVencimientoMapper;
import pe.com.claro.sh.programainformativa.factura.exception.DBException;
import pe.com.claro.sh.programainformativa.factura.util.Constantes;
import pe.com.claro.sh.programainformativa.factura.util.PropertiesExternos;

@Repository
public class OacDaoImpl implements OacDao {

	private static transient Logger logger = Logger.getLogger(OacDaoImpl.class);

	@Autowired
	private PropertiesExternos propertiesExterno;

	@Autowired
	@Qualifier("oacdbDS")
	private DataSource oacdbDS;

	@SuppressWarnings("unchecked")
	@Override
	public ObtenerFacturasEmitidasResponse listarFacturasEmitidas(String msjTransaccion,
			ObtenerFacturasEmitidasRequest request) throws DBException {

		long tiempoInicio = System.currentTimeMillis();
		String msjTx = msjTransaccion + "[listarFacturasEmitidas]";
		ObtenerFacturasEmitidasResponse responseBean = new ObtenerFacturasEmitidasResponse();

		try {
			logger.info(msjTx + Constantes.METODO_DAO_MENSAJE_CONSULTANDO_BD + propertiesExterno.dbOacdb + "]");
			
			SimpleJdbcCall procConsulta1 = new SimpleJdbcCall(oacdbDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbOacdbOwner)
					.withCatalogName(propertiesExterno.dbOacdbPkgOacNotifInfo)
					.withProcedureName(propertiesExterno.dbOacdbSpEaisFacturaEmitida)
					.declareParameters(new SqlParameter(Constantes.SP_OAC_EMISION_RECIBO_PI_PD_FECHA_EMISION, OracleTypes.VARCHAR),
							new SqlOutParameter(Constantes.SP_OAC_EMISION_RECIBO_PO_CURSOR, OracleTypes.CURSOR,new FacturasEmitidasMapper() 
							 ),new SqlOutParameter(Constantes.SP_OAC_EMISION_RECIBO_PO_COD_RPTA, OracleTypes.NUMBER),
							new SqlOutParameter(Constantes.SP_OAC_EMISION_RECIBO_PO_MSG_RPTA, OracleTypes.VARCHAR));

			logger.info(msjTx + Constantes.METODO_DAO_MENSAJE_SE_INVOCARA_SP + propertiesExterno.dbOacdbOwner
					+ Constantes.STRING_PUNTO + propertiesExterno.dbOacdbPkgOacNotifInfo + Constantes.STRING_PUNTO
					+ propertiesExterno.dbOacdbSpEaisFacturaEmitida);
		
			logger.info(msjTx + "PARAMETROS [INPUT]: ");
			logger.info(msjTx + "[PD_FECHA_EMISION] = " + request.getFechaEmision());

			SqlParameterSource objParametrosIN = new MapSqlParameterSource().addValue("PD_FECHA_EMISION",
					request.getFechaEmision());

			procConsulta1.getJdbcTemplate()
					.setQueryTimeout(Integer.parseInt(propertiesExterno.dbOacdbTimeOutExecutionMaxTime));

			Map<String, Object> resultMap = procConsulta1.execute(objParametrosIN);

			logger.info(msjTx + "Se invoco con exito el SP: " + propertiesExterno.dbOacdbOwner
					+ Constantes.STRING_PUNTO + propertiesExterno.dbOacdbPkgOacNotifInfo + Constantes.STRING_PUNTO
					+ propertiesExterno.dbOacdbSpEaisFacturaEmitida);

			
			if (!(resultMap == null || resultMap.isEmpty())) {
				List<CursorFacturasEmitidas> listaCursorFacturasEmitidas= (List<CursorFacturasEmitidas>) resultMap.get(Constantes.SP_OAC_EMISION_RECIBO_PO_CURSOR);
				responseBean.setCodRespuesta((BigDecimal) resultMap.get(Constantes.SP_OAC_EMISION_RECIBO_PO_COD_RPTA));
				responseBean.setMsjRespuesta((String) resultMap.get(Constantes.SP_OAC_EMISION_RECIBO_PO_MSG_RPTA));
				responseBean.setListaFacturas(listaCursorFacturasEmitidas);
				
				logger.info(msjTx + Constantes.METODO_DAO_MENSAJE_PARAMETROS_OUTPUT);
				logger.info(msjTx + "[PO_MSJRPTA] = " + responseBean.getMsjRespuesta());
				logger.info(msjTx + "[PO_CODRPTA] = " + responseBean.getCodRespuesta());
			} else {
				logger.info(msjTx+ "No se encontraro dagtos para procesar");
			}
			
		} catch (NestedRuntimeException e) {
			imprimirLogBD(msjTx, e);
		}
		finally {
			logger.info(msjTx + "Tiempo Transacurrido (ms): [" + (System.currentTimeMillis() - tiempoInicio) + "]");
			logger.info(msjTx + "[FIN] - METODO: [listarFacturasEmitidas - DAO] ");
		}
			
		return responseBean;

	}
	
	
	private void imprimirLogBD(String msjTx, Exception e) throws DBException {
		String codigoError = Constantes.VACIO;
		codigoError = validaExceptionBD(msjTx, e);
		throw new DBException( codigoError, e.getMessage(), propertiesExterno.dbOacdb,
		        propertiesExterno.dbBscsdbOwner + "." + propertiesExterno.dbOacdbPkgOacNotifInfo + "."
		                + propertiesExterno.dbBscsdbSpEaisCargosAdicionales,
		        e );
	}


	private String validaExceptionBD(String msjTx, Exception e) {
		String codigoError;
		if( String.valueOf( e.getMessage() ).contains( Constantes.TEXTO_COULD_NOT_GET_CONNECTION ) ){
		    
		    logger.error( msjTx + "Error de conexion a BD: " + propertiesExterno.dbOacdb+ e );
		    codigoError = Constantes.ERROR_NO_CONEXION;
		}
		else if( String.valueOf( e.getMessage() ).contains( Constantes.ORA_01013 ) ){
		    logger.error( msjTx + "Error de Timeout en BD: " + propertiesExterno.dbOacdb+ e );
		    codigoError = Constantes.ERROR_TIMEOUT;
		}
		else{
		    logger.error( msjTx + "Error general en BD: " + propertiesExterno.dbOacdb+e );
		    codigoError = Constantes.ERROR_GENERAL;
		}
		return codigoError;
	}


	@SuppressWarnings("unchecked")
	@Override
	public ObtenerFechaVencimientoResponse listarFechaVencimiento(String msjTransaccion,
			ObtenerFechaVencimientoRequest request) throws DBException {
		long tiempoInicio = System.currentTimeMillis();
		String msjTx = msjTransaccion + "[listarFacturasEmitidas]";
		ObtenerFechaVencimientoResponse responseBean = new ObtenerFechaVencimientoResponse();

		try {
			logger.info(msjTx + Constantes.METODO_DAO_MENSAJE_CONSULTANDO_BD + propertiesExterno.dbOacdb + "]");
			SimpleJdbcCall procConsulta1 = new SimpleJdbcCall(oacdbDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbOacdbOwner)
					.withCatalogName(propertiesExterno.dbOacdbPkgOacNotifInfo)
					.withProcedureName(propertiesExterno.dbOacdbSpEaisListaVencimiento)
					.declareParameters(
							new SqlParameter(Constantes.SP_OAC_EMISION_RECIBO_PD_FECHA_VENCIMIENTO, OracleTypes.VARCHAR),
							new SqlParameter(Constantes.SP_OAC_EMISION_RECIBO_PD_FECHA_PROX_VENC, OracleTypes.VARCHAR),
						new SqlOutParameter(Constantes.SP_OAC_EMISION_RECIBO_PO_CURSOR, OracleTypes.CURSOR,new FechaVencimientoMapper() 
							 ),
							new SqlOutParameter(Constantes.SP_OAC_EMISION_RECIBO_PO_COD_RPTA, OracleTypes.NUMBER),
							new SqlOutParameter(Constantes.SP_OAC_EMISION_RECIBO_PO_MSG_RPTA, OracleTypes.VARCHAR));

			logger.info(msjTx + Constantes.METODO_DAO_MENSAJE_SE_INVOCARA_SP + propertiesExterno.dbOacdbOwner
					+ Constantes.STRING_PUNTO + propertiesExterno.dbOacdbPkgOacNotifInfo + Constantes.STRING_PUNTO
					+ propertiesExterno.dbOacdbSpEaisListaVencimiento);
		
			logger.info(msjTx + "PARAMETROS [INPUT]: ");
			logger.info(msjTx + Constantes.CORCHETE_INICIO+  Constantes.SP_OAC_EMISION_RECIBO_PD_FECHA_VENCIMIENTO + Constantes.CORCHETE_FIN+request.getFechaVencimiento());
			logger.info(msjTx + Constantes.CORCHETE_INICIO+ Constantes.SP_OAC_EMISION_RECIBO_PD_FECHA_PROX_VENC+ Constantes.CORCHETE_FIN+ request.getFechaProxVencimiento());

			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					.addValue(Constantes.SP_OAC_EMISION_RECIBO_PD_FECHA_VENCIMIENTO,request.getFechaVencimiento())
					.addValue(Constantes.SP_OAC_EMISION_RECIBO_PD_FECHA_PROX_VENC, request.getFechaProxVencimiento());					;
			

			procConsulta1.getJdbcTemplate()
					.setQueryTimeout(Integer.parseInt(propertiesExterno.dbOacdbTimeOutExecutionMaxTime));

			Map<String, Object> resultMap = procConsulta1.execute(objParametrosIN);

			logger.info(msjTx + "Se invoco con exito el SP: " + propertiesExterno.dbOacdbOwner
					+ Constantes.STRING_PUNTO + propertiesExterno.dbOacdbPkgOacNotifInfo + Constantes.STRING_PUNTO
					+ propertiesExterno.dbOacdbSpEaisListaVencimiento);

			
			if (!(resultMap == null || resultMap.isEmpty())) {
				List<CursorFechaVencimiento> listaCursorFechaVencimiento= (List<CursorFechaVencimiento>) resultMap.get(Constantes.SP_OAC_EMISION_RECIBO_PO_CURSOR);
				responseBean.setCodRespuesta((BigDecimal) resultMap.get(Constantes.SP_OAC_EMISION_RECIBO_PO_COD_RPTA));
				responseBean.setMsjRespuesta((String) resultMap.get(Constantes.SP_OAC_EMISION_RECIBO_PO_MSG_RPTA));
				responseBean.setListFechaVencimiento(listaCursorFechaVencimiento);
				
				logger.info(msjTx + Constantes.METODO_DAO_MENSAJE_PARAMETROS_OUTPUT);
				logger.info(msjTx + "[PO_MSJRPTA] = " + responseBean.getMsjRespuesta());
				logger.info(msjTx + "[PO_CODRPTA] = " + responseBean.getCodRespuesta());
			} else {
				logger.info(msjTx+ "No se encontraro dagtos para procesar");
			}
			
		} catch (NestedRuntimeException e) {
			imprimirLogBD(msjTx, e);
		}
		finally {
			logger.info(msjTx + "Tiempo Transacurrido (ms): [" + (System.currentTimeMillis() - tiempoInicio) + "]");
			logger.info(msjTx + "[FIN] - METODO: [listarFacturasEmitidas - DAO] ");
		}
			
		return responseBean;
	}

}
