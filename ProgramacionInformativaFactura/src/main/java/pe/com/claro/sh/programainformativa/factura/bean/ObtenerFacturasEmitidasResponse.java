package pe.com.claro.sh.programainformativa.factura.bean;

 
import java.util.ArrayList;
import java.util.List;

public class ObtenerFacturasEmitidasResponse extends RespuestaBean  {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<CursorFacturasEmitidas> listaFacturas=new ArrayList<>();


	public List<CursorFacturasEmitidas> getListaFacturas() {
		return listaFacturas;
	}


	public void setListaFacturas(List<CursorFacturasEmitidas> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}
	
	
	
		
}
