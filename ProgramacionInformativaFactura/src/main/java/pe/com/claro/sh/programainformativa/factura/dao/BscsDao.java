package pe.com.claro.sh.programainformativa.factura.dao;

import pe.com.claro.sh.programainformativa.factura.bean.RespuestaBean;
import pe.com.claro.sh.programainformativa.factura.exception.DBException;

public interface BscsDao {
	
  RespuestaBean depurarEmsionRecibo (String mensaje) throws DBException;

  RespuestaBean eliminarFactVencidas (String mensaje) throws DBException;
    
	public RespuestaBean consultarMontosAdicionales (String msjTransaccion ) throws  DBException;

	public RespuestaBean consultarFechaVencimiento (String msjTransaccion, String FechaProxima ) throws  DBException;

	public RespuestaBean consultarFechaProxVencimiento (String msjTransaccion, String FechaProxima ) throws  DBException;

}
