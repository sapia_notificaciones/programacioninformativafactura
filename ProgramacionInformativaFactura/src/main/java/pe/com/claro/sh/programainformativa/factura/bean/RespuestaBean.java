package pe.com.claro.sh.programainformativa.factura.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class RespuestaBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal codRespuesta;
	private String msjRespuesta;

	public BigDecimal getCodRespuesta() {
		return codRespuesta;
	}

	public void setCodRespuesta(BigDecimal codRespuesta) {
		this.codRespuesta = codRespuesta;
	}

	public String getMsjRespuesta() {
		return msjRespuesta;
	}

	public void setMsjRespuesta(String msjRespuesta) {
		this.msjRespuesta = msjRespuesta;
	}

}
