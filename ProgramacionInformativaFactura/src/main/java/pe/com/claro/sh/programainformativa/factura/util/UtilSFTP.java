package pe.com.claro.sh.programainformativa.factura.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.commons.vfs.FileSystemException;
import org.apache.commons.vfs.FileSystemOptions;
import org.apache.commons.vfs.provider.sftp.SftpClientFactory;
import org.apache.commons.vfs.provider.sftp.SftpFileSystemConfigBuilder;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

import pe.com.claro.sh.programainformativa.factura.exception.SFTPException;

@Component
public class UtilSFTP {


	private final Logger logger = Logger.getLogger(this.getClass().getName());
	public static String SUCCESS = "1";
	public static String FILE_NOT_FOUND = "2";
	public static String ERROR = "3";

	private ChannelSftp ojbCanalSFTP = null;
	private Session objSesion = null;

	/**
	 * conectarSFTPDMZ1
	 * 
	 * @param trazabilidadParam
	 * @param vServidorSFTP
	 * @param vUsuarioSFTP
	 * @param vPasswordSFTP
	 * @param vPuertoSFTP
	 * @param vTimeOutSFTP
	 * @return boolean
	 * @throws JSchException 
	 * @throws NumberFormatException 
	 * @throws FileSystemException 
	 */
	
	public boolean conectarSftpFileServer(String trazabilidadParam,
			String vServidorSFTP, String vUsuarioSFTP, String vPasswordSFTP,
			int vPuertoSFTP, String vTimeOutSFTP,String canal) {

		String trazabilidad = trazabilidadParam + "[conectarSFTP] ";
		this.logger.info(trazabilidad + "[INICIO] - METODO: [conectarSFTP] ");

		FileSystemOptions objFileSystemOptions = null;
		Channel objCanal = null;
		boolean estadoConexion = false;

		try {

			logger.info(trazabilidad + "- Conectandose al servidor "
					+ vServidorSFTP);

			logger.info(trazabilidad + "- Conectandose al servidor "
					+ vServidorSFTP);

			if (this.ojbCanalSFTP != null) {
				this.logger
						.info(trazabilidad
								+ "La conexion 'SFTP' ya esta en uso, se procede a desconectar...");
				desconectarSFTP(trazabilidad);
			}

			objFileSystemOptions = new FileSystemOptions();

			SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(
					objFileSystemOptions, Constantes.NO_MINUSCULA);

			this.objSesion = SftpClientFactory.createConnection(vServidorSFTP,
					vPuertoSFTP, vUsuarioSFTP.toCharArray(),
					vPasswordSFTP.toCharArray(), objFileSystemOptions);
			objCanal = this.objSesion
					.openChannel(canal);

			this.ojbCanalSFTP = (ChannelSftp) objCanal;

			objCanal.connect(Integer.parseInt(vTimeOutSFTP));

			estadoConexion = this.ojbCanalSFTP.isConnected();
			this.logger.info(trazabilidad + " Conexion creada SFTP ");
		} catch (JSchException e) {
			
			String textoError = Util.getStackTraceFromException(e);
			this.logger
					.error(trazabilidad + "ERROR [FileSystemException]: " + textoError);
			estadoConexion = false;
		} catch (Exception e) {
			this.logger.error(trazabilidad + "ERROR [Exception]: ", e);
			estadoConexion = false;
		} finally {
			this.logger.info(trazabilidad + "[FIN] - METODO: [conectarSFTP] ");
		}

		return estadoConexion;
	}

	/**
	 * desconectarSFTP
	 * 
	 * @param trazabilidadParam
	 * @return boolean
	 */
	public boolean desconectarSFTP(String trazabilidadParam) {

		String trazabilidad = trazabilidadParam + "[desconectarSFTP]";
		this.logger
				.info(trazabilidad + "[INICIO] - METODO: [desconectarSFTP] ");

		boolean estadoConexion = false;

		try {
			if (this.ojbCanalSFTP != null) {
				this.ojbCanalSFTP.exit();
			}

			if (this.objSesion != null) {
				this.objSesion.disconnect();
			}

			this.ojbCanalSFTP = null;
			estadoConexion = true;
		} catch (Exception e) {
			this.logger.error(trazabilidad + "ERROR [Exception]: ", e);
		} finally {
			this.logger.info(trazabilidad
					+ "[FIN] - METODO: [desconectarSFTP] ");
		}

		return estadoConexion;
	}

	
    public boolean enviarArchivoSftpFileServer(String trazabilidadParam,String rutaOrigen,
			 String rutaRemota, List<String> listaArchivos
			) throws SFTPException {
		String trazabilidad = trazabilidadParam
				+ "[enviarArchivoSftpFileServer]";
		boolean estadoSubida = false;
		this.logger.info(trazabilidad
				+ "[INICIO] - METODO: [cargarArchivoSFTP] ");
		try {
			
		    this.logger.info( trazabilidad + "cantidad de archivos a enviar : "+ listaArchivos.size());
		    
		    for( int i = 0; i < listaArchivos.size(); i++ ){
		        this.logger.info(trazabilidad + "Ruta Origen: " + rutaOrigen+listaArchivos.get( i ));
		        this.logger.info(trazabilidad + "Ruta Destino: " + rutaRemota);
		        this.logger.info(trazabilidad + "Cargando archivo...");
		        
		        
		        this.ojbCanalSFTP.cd(rutaRemota);
                
		        this.ojbCanalSFTP.put(rutaOrigen+ listaArchivos.get( i ), rutaRemota+listaArchivos.get( i ));
                    estadoSubida=true;
                
		        
            }
		    
		 } catch (SftpException e) {
			this.logger.error(trazabilidad + "ERROR [SftpException]: ", e);
			estadoSubida=false;

		} catch (Exception e) {
			logger.error(trazabilidad + "ERROR enviando el archivo al FTP: ", e);
			throw new SFTPException(rutaRemota, rutaOrigen);

		} finally {
			if (this.ojbCanalSFTP != null) {
				this.ojbCanalSFTP.exit();
			}

			if (this.objSesion != null) {
				this.objSesion.disconnect();
			}
		}

		return estadoSubida;

	}

	/**
	 * obtenerArchivoSFTP
	 * 
	 * @param vRutaRemotaArchivo
	 * @return ArchivoAsurionBean
	 */
	public List<String> obtenerArchivoSFTP(String mensajeTransaccion,
	        String vExtensionArchivo,
			String vRutaRemotaArchivo, String vRutaDestino,
			String vArchivoRemoto,  String vServidor)
			throws SFTPException {

		long tiempoInicio = System.currentTimeMillis();

		List<String> listaArchivos = new ArrayList<>();
		
		SftpATTRS existeArchivo =null;
		
		String trazabilidad = mensajeTransaccion + "[obtenerArchivoSFTP]";
		this.logger.info(trazabilidad
				+ "[INICIO] - METODO: [obtenerArchivoSFTP ] ");
		
		try {
			
			this.logger.info(trazabilidad + "ruta del origen :  "
					+ vRutaRemotaArchivo);
			
			this.logger.info(trazabilidad + " Directorio Remoto : "
					+  vRutaDestino);

			
				existeArchivo=this.ojbCanalSFTP.lstat(vRutaRemotaArchivo);
				
				listarArchivosFTP(vExtensionArchivo, vRutaRemotaArchivo, vRutaDestino, vArchivoRemoto, listaArchivos,
						existeArchivo, trazabilidad);
				
			

		} catch (Exception e) {
			throw new SFTPException(vServidor	, vRutaRemotaArchivo, vArchivoRemoto, e);
			
		}

		finally {
			this.logger.info(trazabilidad + "Tiempo Transcurrido (ms): ["
					+ (System.currentTimeMillis() - tiempoInicio) + "]");
			this.logger.info(trazabilidad
					+ "[FIN] - METODO: [ObtenerArchivoSFTP - SFTP]");
		}

		return listaArchivos;

	}

	@SuppressWarnings("unchecked")
	private void listarArchivosFTP(String vExtensionArchivo, String vRutaRemotaArchivo, String vRutaDestino,
			String vArchivoRemoto, List<String> listaArchivos, SftpATTRS existeArchivo, String trazabilidad)
			throws SftpException {
		String obtenerArchivo;
		Vector<ChannelSftp.LsEntry> list;
		if (existeArchivo !=null) {

			this.logger.info(trazabilidad + "Obteniendo archivo...");
			this.ojbCanalSFTP.cd(vRutaRemotaArchivo);
			list=this.ojbCanalSFTP.ls(vArchivoRemoto+"*");
			
			if (list.isEmpty()) {
				
				logger.info(trazabilidad+  vArchivoRemoto
						+ " No se ha encontrado archivo en directorio remoto :  "
						+ vRutaRemotaArchivo);
			} else{
			    
			    for( ChannelSftp.LsEntry lsEntry: list ){
			        this.logger.info(trazabilidad + "file: " + lsEntry.getFilename());
			        obtenerArchivo = lsEntry.getFilename();
			        obtenerListarFicheroFTP(vExtensionArchivo, vRutaRemotaArchivo, vRutaDestino, vArchivoRemoto,
							listaArchivos, trazabilidad, obtenerArchivo);
			        
		        }
			    
			  	obtenerArchivo=vRutaDestino+vArchivoRemoto;
			}
			
		} else {
			this.logger.info(trazabilidad + "Carpeta no existe");
			obtenerArchivo=vRutaDestino+"No existe";
			
		}
	}

	private void obtenerListarFicheroFTP(String vExtensionArchivo, String vRutaRemotaArchivo, String vRutaDestino,
			String vArchivoRemoto, List<String> listaArchivos, String trazabilidad, String obtenerArchivo)
			throws SftpException {
		if( obtenerArchivo.endsWith( vExtensionArchivo ) ){
		    logger.info( trazabilidad+ "Nombre de archivo remoto  : "+ obtenerArchivo );
		    this.ojbCanalSFTP.get( vRutaRemotaArchivo+obtenerArchivo,vRutaDestino+obtenerArchivo );
		    listaArchivos.add( obtenerArchivo );
		    logger.info( trazabilidad+ "Eliminando archivo " + obtenerArchivo);
		    this.ojbCanalSFTP.rm( vRutaRemotaArchivo+obtenerArchivo );
		    this.logger.info(trazabilidad + "archivo : "
		            + vArchivoRemoto + " guardado correctamente en :  "
		            + vRutaDestino);
		}
	}
	
}
