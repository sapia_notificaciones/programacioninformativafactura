package pe.com.claro.sh.programainformativa.factura.bean;

public class ObtenerFechaVencimientoRequest {
	private String fechaVencimiento; 
	private String fechaProxVencimiento;
	
	
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getFechaProxVencimiento() {
		return fechaProxVencimiento;
	}
	public void setFechaProxVencimiento(String fechaProxVencimiento) {
		this.fechaProxVencimiento = fechaProxVencimiento;
	}
	
	

}
