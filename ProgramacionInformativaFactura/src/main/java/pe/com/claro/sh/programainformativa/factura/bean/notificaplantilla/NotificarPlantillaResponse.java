package pe.com.claro.sh.programainformativa.factura.bean.notificaplantilla;

public class NotificarPlantillaResponse {

	
	private String codigoResultado;

	
	private String mensajeResultado;


    
    public String getCodigoResultado(){
        return codigoResultado;
    }


    
    public void setCodigoResultado( String codigoResultado ){
        this.codigoResultado = codigoResultado;
    }


    
    public String getMensajeResultado(){
        return mensajeResultado;
    }


    
    public void setMensajeResultado( String mensajeResultado ){
        this.mensajeResultado = mensajeResultado;
    }

	

}
