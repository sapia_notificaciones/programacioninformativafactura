package pe.com.claro.sh.programainformativa.factura.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.claro.sh.programainformativa.factura.bean.CursorFacturasEmitidas;
import pe.com.claro.sh.programainformativa.factura.bean.CursorFechaVencimiento;
import pe.com.claro.sh.programainformativa.factura.bean.ObtenerFacturasEmitidasRequest;
import pe.com.claro.sh.programainformativa.factura.bean.ObtenerFacturasEmitidasResponse;
import pe.com.claro.sh.programainformativa.factura.bean.ObtenerFechaVencimientoRequest;
import pe.com.claro.sh.programainformativa.factura.bean.ObtenerFechaVencimientoResponse;
import pe.com.claro.sh.programainformativa.factura.bean.RespuestaBean;
import pe.com.claro.sh.programainformativa.factura.bean.notificaplantilla.Archivo;
import pe.com.claro.sh.programainformativa.factura.bean.notificaplantilla.NotificarPlantillaRequest;
import pe.com.claro.sh.programainformativa.factura.bean.notificaplantilla.NotificarPlantillaResponse;
import pe.com.claro.sh.programainformativa.factura.dao.BscsDao;
import pe.com.claro.sh.programainformativa.factura.dao.OacDao;
import pe.com.claro.sh.programainformativa.factura.exception.DBException;
import pe.com.claro.sh.programainformativa.factura.exception.SFTPException;
import pe.com.claro.sh.programainformativa.factura.exception.WSException;
import pe.com.claro.sh.programainformativa.factura.proxy.NotificacionInformativa;
import pe.com.claro.sh.programainformativa.factura.util.Constantes;
import pe.com.claro.sh.programainformativa.factura.util.PropertiesExternos;
import pe.com.claro.sh.programainformativa.factura.util.UtilSFTP;
import pe.com.claro.sh.programainformativa.factura.util.Utilitarios;

@Service
public class ProgramaNotificacionInformativaServiceImpl implements ProgramaNotificacionInformativaService {

	private static Logger log = Logger.getLogger(ProgramaNotificacionInformativaServiceImpl.class);
	@Autowired
	private PropertiesExternos propertiesExternos;
	@Autowired
	private UtilSFTP utilSFTP;
	@Autowired
	private Utilitarios utilitarios;
	@Autowired
	private BscsDao bscsDao;
	@Autowired
	private OacDao oacDao;
	@Autowired
	private NotificacionInformativa notificacionInformativa;

	@Override
	public void procesar(String msTxIn, String idTransaccion, String idPlantilla) {

		String msjTx = msTxIn + "[programaNotificacionInformativaService] ";
		log.info(msTxIn + "[Actividad 1-  Validar codigo de Plantilla - Inicio]");
		
		if (idPlantilla==null || idPlantilla.isEmpty()) {
			log.info(msjTx
					+ "Datos para ejecución de corteReconexion incorrectos");
			return;
		}else{
			validarPlantilla(msjTx, idTransaccion, idPlantilla);
			
		}
		
		
	}

	private void terminarConexiones(String mensajeTransaccion) {
		log.info(mensajeTransaccion + " Cerrando conexiones ... ");
		utilSFTP.desconectarSFTP(mensajeTransaccion);
		log.info(mensajeTransaccion + " Conexiones cerradas ... ");
	}

	public void validarPlantilla(String msTxIn, String idTransaccion, String plantilla) {
		String varPlantilla = propertiesExternos.dbNotifdbPlantilla;
		String[] arregloPlantilla = varPlantilla.split(Constantes.SEPARADOR);
		log.info(msTxIn + "[Actividad 1-  Validar codigo de Plantilla - Fin]");
		String plantillaFacturasEmitidas = arregloPlantilla[Constantes.INT_UNO];
		String plantillaFechaVencimiento = arregloPlantilla[Constantes.INT_DOS];
		String plantillaVencimientoProximo = arregloPlantilla[Constantes.INT_TRES];
		
		if (plantilla.equalsIgnoreCase(plantillaFechaVencimiento)) {
			plantillaFechaVencimiento(msTxIn, idTransaccion, plantilla);
			return;
		} else if (plantilla.equalsIgnoreCase(plantillaVencimientoProximo)) {
			plantillaVencimientoProximo(msTxIn,idTransaccion, plantilla);
			return;
		}  else if (plantilla.equalsIgnoreCase(plantillaFacturasEmitidas)) {
			plantillaEmisionRecibo(msTxIn, idTransaccion, plantilla);
			return;
		} else {
			log.info(msTxIn + "No existe plantilla : " + plantilla);
			return;
		}
	}

	public void plantillaEmisionRecibo(String msjTx, String idTransaccion, String idPlantilla) {
		log.info(msjTx + " valor de plantilla : " + idPlantilla);
		ObtenerFacturasEmitidasResponse obtenerFacturasEmitidasResponse = new ObtenerFacturasEmitidasResponse();
		log.info(msjTx+ "Actividad 4 - Inicio");
		try {

			RespuestaBean eliminarRespuesta = new RespuestaBean();
			log.info(msjTx + " [Actividad 4.1 - Depurar registros de la tabla temporal "+ Constantes.TABLA_TEMPORAL +" Inicio] ");
			
			eliminarRespuesta = bscsDao.depurarEmsionRecibo(msjTx);

			log.info(msjTx + " [Actividad 4.1 - Depurar registros de la tabla temporal "+ Constantes.TABLA_TEMPORAL +" Fin] ");

			if (String.valueOf(eliminarRespuesta.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_0)) {

				log.info(msjTx + Constantes.MENSAJE_EXITO + "[ : BD BSCS]");
			} else {
				log.info(msjTx + " Ocurrio un error al eliminar facturas de emision de recibo ");
				return;
			}

			log.info(msjTx + "[Actividad 4.2 - Listar Facturas  emision  recibo Inicio]");

			String fechaOAC = utilitarios.formatoFechaReducida(propertiesExternos.dbOacdbFormatoFecha,
					propertiesExternos.dbOacdbRangoFecha);

			ObtenerFacturasEmitidasRequest obtenerFacturasEmitidasRequest = new ObtenerFacturasEmitidasRequest();
			obtenerFacturasEmitidasRequest.setFechaEmision(fechaOAC);

			obtenerFacturasEmitidasResponse = oacDao.listarFacturasEmitidas(msjTx, obtenerFacturasEmitidasRequest);

			List<CursorFacturasEmitidas> listaTotalFacturas = obtenerFacturasEmitidasResponse.getListaFacturas();
			int tamanoCursor = obtenerFacturasEmitidasResponse.getListaFacturas().size();

			log.info(msjTx + "Se obtuvo la lista con " + tamanoCursor + Constantes.REGISTROS);

			log.info(msjTx + "[Actividad 4.2 - Listar Facturas  emision  recibo Fin]");

			if (String.valueOf(obtenerFacturasEmitidasResponse.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_0)) {
				log.info(msjTx + " Operacion exitosa en la ejecucion de la bd " + propertiesExternos.dbOacdb);

			} else {
				log.info(msjTx + " Ocurrio un error las factura emitidas");
			}

			String[] filtro = propertiesExternos.vRutaLocalEmisionFiltro.split(Constantes.SEPARADOR);

			log.info(msjTx + "[Actividad 4.3 - Generar  base de Facturas  con Emision recibo - Inicio]");

			String rutaNombreArchivo = propertiesExternos.vRutaDataCtlFactura + propertiesExternos.LOCAL_NOMBRE_ARCHIVO
					+ propertiesExternos.LOCAL_EXTENSION_ARCHIVO;

			String filtroBSCS = filtro[Constantes.INT_CERO];
			String filtroSGA = filtro[Constantes.INT_UNO];
			List<String> listafacturasBSCS = new ArrayList<String>();
			List<String> listafacturasSGA = new ArrayList<String>();

			cargaOrigenFactura(msjTx, listaTotalFacturas, filtroBSCS, filtroSGA, listafacturasBSCS, listafacturasSGA);

			log.info(msjTx + "Se obtuvo la Facturas de BSCS con " + listafacturasBSCS.size() + Constantes.REGISTROS);
			log.info(msjTx + "Se obtuvo la Facturas de SGA con " + listafacturasSGA.size() + Constantes.REGISTROS);

			File archivoFileBaseBSCS = utilitarios.generarArchivo(msjTx, rutaNombreArchivo, listafacturasBSCS);

			log.info(msjTx + "[Actividad 4.3 - Generar  base de Facturas  con Emision recibo - Fin]");

			String fechaCTL = utilitarios.formatoFecha(propertiesExternos.pFORMATO_FECHA_NOMBRE);

			String rutaCTL = utilitarios.armarRutaCTL(propertiesExternos.vRutaCtlUserApp,
					propertiesExternos.pNOMBRE_ARCHIVO_FACTURA_CTL, propertiesExternos.pEXTENSION_ARCHIVO_CTL);

			String rutaBad = utilitarios.armarSentenciaCTL(fechaCTL, propertiesExternos.vRutaBadCtlFactura,
					propertiesExternos.pNOMBRE_ARCHIVO_FACTURA_CTL, propertiesExternos.pEXTENSION_ARCHIVO_BAD);

			String rutaLOGCTL = utilitarios.armarSentenciaCTL(fechaCTL, propertiesExternos.vRutaLogCtlFactura,
					propertiesExternos.pNOMBRE_ARCHIVO_FACTURA_CTL, propertiesExternos.pEXTENSION_ARCHIVO_LOG);

			String rutaDiscard = utilitarios.armarSentenciaCTL(fechaCTL, propertiesExternos.vRutaDiscarCtlFactura,
					propertiesExternos.pNOMBRE_ARCHIVO_FACTURA_CTL, propertiesExternos.pEXTENSION_ARCHIVO_DSC);

			RespuestaBean cargaFacturaBSCS = new RespuestaBean();

			  
			log.info(msjTx + "Actividad 4.4  - Registrar archivo plano en la tabla " +Constantes.TABLA_TEMPORAL+ " - Inicio");

			cargaFacturaBSCS = utilitarios.ejecutarSqlldr(msjTx, rutaBad, rutaLOGCTL, archivoFileBaseBSCS, rutaCTL,
					rutaDiscard, propertiesExternos.userBSCSCTL, propertiesExternos.passBSCSCTL,
					propertiesExternos.dbBSCSCTL, propertiesExternos.perrorsCTL, propertiesExternos.pBindSizeCTL,
					propertiesExternos.pReadSizeCTL, propertiesExternos.prowsCTL);

			log.info(msjTx + "Actividad 4.4  - Registrar archivo plano en la tabla " +Constantes.TABLA_TEMPORAL+ " - Fin");

			if (String.valueOf(cargaFacturaBSCS.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_0)) {

				log.info(msjTx + " Se cargo " + archivoFileBaseBSCS + " en " + Constantes.TABLA_TEMPORAL);

			} else {
				log.error(msjTx + "ocurrio un error en la carga revisar log " + rutaLOGCTL);
				return;
			}

			log.info(msjTx + "[Depurar Fichero temporal ]");

			utilitarios.eliminarFichero(msjTx, archivoFileBaseBSCS);


			log.info(msjTx + "[Actividad 4.5 - Consultar  Base de emision recibo - Inicio]");
			RespuestaBean responseBSCS = bscsDao.consultarMontosAdicionales(msjTx);
			log.info(msjTx + "[Actividad 4.5 - Consultar  Base de emision recibo - Fin]");

			List<String> listaArchivo = new ArrayList<>();
			if (String.valueOf(responseBSCS.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_0)) {

				log.info(msjTx + "[Actividad 4.6 -  Obtener archivo Base de Factura de Emision Recibo- Inicio]");
				listaArchivo = obtenerArchivoPlanoUsurio(msjTx);

				if (listaArchivo != null) {
					log.info(msjTx + " Archivo obtenido " + listaArchivo.get(Constantes.INT_CERO));
					log.info(msjTx + " Archivo obtenido " + listaArchivo.get(Constantes.INT_UNO));
				}

				log.info(msjTx + "[Actividad 4.6 -  Obtener archivo Base de Factura de Emision Recibo- Fin]");
			} else {
				log.info(msjTx + " No se encontraron fichero en BSCS ");
				return;
			}

			log.info(msjTx + "Validando Peso archivo ");

			String rutaOrigen = propertiesExternos.vRutaLocalEmisionRecibo;
			String rutaEnvio = propertiesExternos.vRutaPendienteEnvioEmision;
			String extensionArchivo = propertiesExternos.vEmisionReciboExtension;
			String filtroArchivo = propertiesExternos.CONEXION_SFTP_BSCS_ARCHIVO_BASE;
			enviarArchivo(msjTx, idTransaccion, listaArchivo, rutaOrigen, rutaEnvio, extensionArchivo, filtroArchivo);

		} catch (WSException e) {
			log.error(msjTx + "Error WS", e);
			log.info(msjTx + Constantes.CODIGO_ERROR + e.getCodError());
			log.info(msjTx + Constantes.MENSAJE_ERROR + e.getMsjError());
		} catch (DBException e) {
			log.error(msjTx + "Error BD", e);
			log.info(msjTx + Constantes.CODIGO_ERROR + e.getCodError());
			log.info(msjTx + Constantes.MENSAJE_ERROR + e.getMsjError());

		} catch (SFTPException e) {
			log.error(msjTx + "Error SFTP", e);
			log.info(msjTx + Constantes.CODIGO_ERROR + e.getCodError());
			log.info(msjTx + "[MSGERROR]: " + e.getMsjError());
		} catch (Exception e) {
			log.error(msjTx + "Error inesperado:" + e.getMessage(), e);
		}
	}
	
	private void enviarArchivoFecha(String msjTx, String idTransaccion, List<String> listaArchivo, String rutaOrigen,
			String rutaEnvio, String extensionArchivo, String filtroArchivo, String plantilla) throws SFTPException, WSException {
			String valorRandon;
			int cantidadFicheros;
			
			for (int i = 0; i < listaArchivo.size(); i++) {
				
				
				valorRandon = utilitarios.generarAleatorio(Constantes.VALOR_RANDON);

				log.info(msjTx + "renombrar archivo : " + listaArchivo.get(i));
				String nombreFechVenc=listaArchivo.get(i);
				String ficheroRenombrado = renombrarArchivo(msjTx, idTransaccion, valorRandon, rutaOrigen,
						nombreFechVenc);

				log.info(msjTx + " [Validar TamanoArchivo]");
				
				double pesoArchivo = validarPesoArchivo(msjTx, rutaOrigen, ficheroRenombrado);
				log.info(msjTx + "[archivo] " + ficheroRenombrado + " pesa : " + pesoArchivo + Constantes.TAMANO_MEGA);

				File fichero = new File(rutaOrigen + ficheroRenombrado);
				long cantidadRegistros = utilitarios.obtenerNumeroRegistros(msjTx, rutaOrigen, ficheroRenombrado);
				
				log.info(msjTx + "Archivo" + ficheroRenombrado + " contiene " + cantidadRegistros + " registros");
				
				
				if (pesoArchivo <= propertiesExternos.TAMANO_MAXIMO_ARCHIVO) {
					log.info(msjTx + "mover nombre archivo : " + ficheroRenombrado);

					utilitarios.moverArchivLocal(msjTx, fichero, rutaEnvio, filtroArchivo);

				} else {
					
					log.info(msjTx + " [Actividad  Particionar fichero  en repositorio Local Inicio] ");
					
					cantidadFicheros = utilitarios.obtenerCantidadArchivo(propertiesExternos.TAMANO_MAXIMO_ARCHIVO,
							cantidadRegistros);
					
					log.info(msjTx + "archivo " + ficheroRenombrado + " se divira en : " + cantidadFicheros);
					
					RespuestaBean dividirResponse = new RespuestaBean();

					String archivoRenombrado = ficheroRenombrado.substring(Constantes.INT_CERO,
							ficheroRenombrado.indexOf(Constantes.STRING_PUNTO)) + Constantes.GUION_ABAJO;
					dividirResponse = utilitarios.dividirArchivo(msjTx, rutaOrigen, ficheroRenombrado, archivoRenombrado,
							extensionArchivo, (long) propertiesExternos.CANTIDAD_LINEAS);

					if (String.valueOf(dividirResponse.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_0)) {

						log.info(msjTx + " Operacion exitosa");
						log.info(msjTx + " Mover a carpeta por enviar ");
						log.info(msjTx + "eliminar archivo : " + ficheroRenombrado);
						File archivoEliminar = new File(rutaOrigen + ficheroRenombrado);
						utilitarios.eliminarFichero(msjTx, archivoEliminar);

						utilitarios.moverArchivoBloque(msjTx, rutaOrigen, rutaEnvio, extensionArchivo, archivoRenombrado,
								cantidadFicheros);
					} else {
						log.error(msjTx + "ocurrio un error al dividir archivo " + ficheroRenombrado);
					}

				}

			}
			ficheroEnviadosVenc(msjTx, rutaEnvio, filtroArchivo, plantilla);
		}
	
	
	private void enviarArchivo(String msjTx, String idTransaccion, List<String> listaArchivo, String rutaOrigen,
			String rutaEnvio, String extensionArchivo, String filtroArchivo) throws SFTPException, WSException {
		String valorRandon;
		int cantidadFicheros;
		for (int i = 0; i < listaArchivo.size(); i++) {
			valorRandon = utilitarios.generarAleatorio(Constantes.VALOR_RANDON);

			log.info(msjTx + "renombrar archivo : " + listaArchivo.get(i));

			String ficheroRenombrado = renombrarArchivo(msjTx, idTransaccion, valorRandon, rutaOrigen,
					listaArchivo.get(i));

			double pesoArchivo = validarPesoArchivo(msjTx, rutaOrigen, ficheroRenombrado);
			log.info(msjTx + "[archivo] " + ficheroRenombrado + " pesa : " + pesoArchivo + Constantes.TAMANO_MEGA);

			File fichero = new File(rutaOrigen + ficheroRenombrado);
			if (pesoArchivo <= propertiesExternos.TAMANO_MAXIMO_ARCHIVO) {
				log.info(msjTx + "mover nombre archivo : " + ficheroRenombrado);

				utilitarios.moverArchivLocal(msjTx, fichero, rutaEnvio, filtroArchivo);

			} else {
				log.info(msjTx + " [Actividad 8- Particionar fichero  en repositorio Local Inicio] ");
				cantidadFicheros = utilitarios.obtenerCantidadArchivo(propertiesExternos.TAMANO_MAXIMO_ARCHIVO,
						(new Double(pesoArchivo)).longValue());

				log.info(msjTx + "archivo " + ficheroRenombrado + " se divira en : " + cantidadFicheros);

				RespuestaBean dividirResponse = new RespuestaBean();

				String archivoRenombrado = ficheroRenombrado.substring(Constantes.INT_CERO,
						ficheroRenombrado.indexOf(Constantes.STRING_PUNTO)) + Constantes.GUION_ABAJO;
				dividirResponse = utilitarios.dividirArchivo(msjTx, rutaOrigen, ficheroRenombrado, archivoRenombrado,
						extensionArchivo, (long) propertiesExternos.CANTIDAD_LINEAS);

				if (String.valueOf(dividirResponse.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_0)) {

					log.info(msjTx + " Operacion exitosa");
					log.info(msjTx + " Mover a carpeta por enviar ");
					log.info(msjTx + "eliminar archivo : " + ficheroRenombrado);
					File archivoEliminar = new File(rutaOrigen + ficheroRenombrado);
					utilitarios.eliminarFichero(msjTx, archivoEliminar);

					utilitarios.moverArchivoBloque(msjTx, rutaOrigen, rutaEnvio, extensionArchivo, archivoRenombrado,
							cantidadFicheros);
				} else {
					log.error(msjTx + "ocurrio un error al dividir archivo " + ficheroRenombrado);
				}

			}

		}

		ficheroEnviados(msjTx, rutaEnvio, filtroArchivo);
	}
	
	private void ficheroEnviadosVenc(String msjTx, String rutaEnvio, String filtroArchivo,String plantilla)
			throws SFTPException, WSException {
		List<String> listaFicherosPorEnviar;
		listaFicherosPorEnviar = utilitarios.listarArchivos(msjTx, rutaEnvio, filtroArchivo);

		int cantpArchProceso = listaFicherosPorEnviar.size();

		if (listaFicherosPorEnviar == null | listaFicherosPorEnviar.isEmpty()) {
			log.info(msjTx + " No se encontraron fichero a procesar");
		} else {
			boolean enviarFicheros = enviarArchivoPlano(msjTx, rutaEnvio, listaFicherosPorEnviar);

			if (enviarFicheros) {
				log.info(msjTx + "Se transferio  SFTP");
				
			} else {
				log.info(msjTx + "Ocurrio error durante la transferencia");
			}

		}
		terminarConexiones(msjTx);
		for (String string : listaFicherosPorEnviar) {
			File archivoEliminar = new File(rutaEnvio + string);
			utilitarios.eliminarFichero(msjTx, archivoEliminar);
		}

		log.info(msjTx + "[Actividad 7-  Enviar Notificacion - Inicio]");
		
		for( int i = 0; i < cantpArchProceso; i++ ){
		
		 String idTransaccionRest=utilitarios.generarIdTransaccionFichero(
		 listaFicherosPorEnviar.get(i) );
		 String fechaRest=utilitarios.formatearFecha( msjTx,
		 Constantes.FORMATO_FECHA_REST);
		 String userId=propertiesExternos.wsNotificaInformativaUserId ;
		 NotificarPlantillaRequest notificRequest = new  NotificarPlantillaRequest();
		 notificRequest=armarRequestNotificaRestFech
		 ( listaFicherosPorEnviar.get( i ),
		 propertiesExternos.wsNotificaInformativaUbicacion,plantilla );
		
		
		 NotificarPlantillaResponse notificResponse = new
		 NotificarPlantillaResponse();
		 notificResponse=notificacionInformativa.notificarPlantilla(
		 msjTx,fechaRest ,idTransaccionRest,userId ,notificRequest );
		
		 log.info( msjTx + "[Actividad 7- Enviar Notificacion - Fin]" );
		 if( notificResponse.getCodigoResultado().equalsIgnoreCase(
		 Constantes.CODIGO_EXITO_0 ) ){
		
		 log.info( msjTx + "Operacion exitosa" );
		 }
		 else{
		
		 log.info( msjTx + "Ocurrio un error durante la ejecucion" );
		 }
		 }
	}

	private void ficheroEnviados(String msjTx, String rutaEnvio, String filtroArchivo)
			throws SFTPException, WSException {
		List<String> listaFicherosPorEnviar;
		listaFicherosPorEnviar = utilitarios.listarArchivos(msjTx, rutaEnvio, filtroArchivo);

		int cantpArchProceso = listaFicherosPorEnviar.size();

		if (listaFicherosPorEnviar == null | listaFicherosPorEnviar.isEmpty()) {
			log.info(msjTx + " No se encontraron fichero a procesar");
		} else {
			boolean enviarFicheros = enviarArchivoPlano(msjTx, rutaEnvio, listaFicherosPorEnviar);

			if (enviarFicheros) {
				log.info(msjTx + "Se transferio  SFTP");
				
			} else {
				log.info(msjTx + "Ocurrio error durante la transferencia");
			}

		}
		terminarConexiones(msjTx);
		for (String string : listaFicherosPorEnviar) {
			File archivoEliminar = new File(rutaEnvio + string);
			utilitarios.eliminarFichero(msjTx, archivoEliminar);
		}

		String varPlantilla = propertiesExternos.dbNotifdbPlantilla;
		String[] arregloPlantilla = varPlantilla.split(Constantes.SEPARADOR);
		String idPlantillaSinCargo = arregloPlantilla[Constantes.INT_UNO];
		String idPlantillaConCargo = arregloPlantilla[Constantes.INT_CERO];
		log.info(msjTx + "[Actividad 7-  Enviar Notificacion - Inicio]");
		 for( int i = 0; i < cantpArchProceso; i++ ){
		
		 String idTransaccionRest=utilitarios.generarIdTransaccionFichero(
		 listaFicherosPorEnviar.get(i) );
		 String fechaRest=utilitarios.formatearFecha( msjTx,
		 Constantes.FORMATO_FECHA_REST);
		 String userId=propertiesExternos.wsNotificaInformativaUserId ;
		 NotificarPlantillaRequest notificRequest = new
		 NotificarPlantillaRequest();
		 notificRequest=armarRequestNotificaRest
		 ( listaFicherosPorEnviar.get( i ),
		 propertiesExternos.wsNotificaInformativaUbicacion,idPlantillaSinCargo,
		 idPlantillaConCargo );
		
		
		 NotificarPlantillaResponse notificResponse = new
		 NotificarPlantillaResponse();
		 notificResponse=notificacionInformativa.notificarPlantilla(
		 msjTx,fechaRest ,idTransaccionRest,userId ,notificRequest );
		
		 log.info( msjTx + "[Actividad 7- Enviar Notificacion - Fin]" );
		 if( notificResponse.getCodigoResultado().equalsIgnoreCase(
		 Constantes.CODIGO_EXITO_0 ) ){
		
		 log.info( msjTx + "Operacion exitosa" );
		 }
		 else{
		
		 log.info( msjTx + "Ocurrio un error durante la ejecucion" );
		 }
		 }
	}

	private double validarPesoArchivo(String msjTx, String ruta, String archivo) {

		double pesoArchivo = 0;

		File archivoMega = new File(ruta + archivo);

		pesoArchivo = utilitarios.obtenerPesoArchivoMega(msjTx, archivoMega);

		return pesoArchivo;

	}

	private String renombrarArchivo(String msjTx, String idTransaccion, String randon, String ruta, String archivo) {

		String nuevoArchivo = idTransaccion + randon + Constantes.GUION_ABAJO + archivo;
		File ficheroAntigupo = new File(ruta + archivo);
		File ficheroNuevo = new File(ruta + nuevoArchivo);

		boolean operacion = utilitarios.renombrarArchivo(idTransaccion, ficheroAntigupo, ficheroNuevo);
		if (operacion) {
			log.info(msjTx + "Operacion exitosa para renombrar archivo : " + archivo + " a " + nuevoArchivo);
		} else {
			log.info(msjTx + "Ocurrio  un error al renombrar  :" + archivo + "a" + nuevoArchivo);
		}
		return nuevoArchivo;

	}

	public void cargaFacturasVencidas(String msjTx, List<CursorFechaVencimiento> listaTotalFacturas, String filtroBSCS,
			String filtroSGA, List<String> listafacturasBSCS, List<String> listafacturasSGA) {
		for (CursorFechaVencimiento cursorFacturasEmitidas : listaTotalFacturas) {
			String facturasBSCS = null, facturasSGA = null;
			if (cursorFacturasEmitidas.getOrigen().equalsIgnoreCase(filtroBSCS)) {
				facturasBSCS = cursorFacturasEmitidas.getFactura();
				listafacturasBSCS.add(facturasBSCS);
			} else if (cursorFacturasEmitidas.getOrigen().equalsIgnoreCase(filtroSGA)) {
				facturasSGA = cursorFacturasEmitidas.getFactura();
				listafacturasSGA.add(facturasSGA);
			} else {
				log.info(msjTx + "Origen de Factura Diferente a SGA O BSCS :  ");
				return;
			}
		}
	}

	public void cargaOrigenFactura(String msjTx, List<CursorFacturasEmitidas> listaTotalFacturas, String filtroBSCS,
			String filtroSGA, List<String> listafacturasBSCS, List<String> listafacturasSGA) {
		for (CursorFacturasEmitidas cursorFacturasEmitidas : listaTotalFacturas) {
			String facturasBSCS = null, facturasSGA = null;
			if (cursorFacturasEmitidas.getOrigenFactura().equalsIgnoreCase(filtroBSCS)) {
				facturasBSCS = cursorFacturasEmitidas.getFactura();
				listafacturasBSCS.add(facturasBSCS);
			} else if (cursorFacturasEmitidas.getOrigenFactura().equalsIgnoreCase(filtroSGA)) {
				facturasSGA = cursorFacturasEmitidas.getFactura();
				listafacturasSGA.add(facturasSGA);
			} else {
				log.info(msjTx + "Origen de Factura Diferente a SGA O BSCS :  ");
				return;
			}
		}
	}

	private boolean enviarArchivoPlano(String mensajeTransaccion, String rutaOrigen, List<String> archivoLocal)
			throws SFTPException {
		boolean conexionFileServer = false;
		boolean estadoEnvio = false;
		String rutaDestino = propertiesExternos.FileServerRutaRemota;
		String servidor = propertiesExternos.FileServerIP;
		String usuario = propertiesExternos.FileServerUsuario;
		String password = propertiesExternos.FileServerPass;
		int puerto = propertiesExternos.FileServerPuerto;
		String timeout = propertiesExternos.FileServertimeOut;

		try {
			conexionFileServer = utilSFTP.conectarSftpFileServer(mensajeTransaccion, servidor, usuario, password,
					puerto, timeout, propertiesExternos.FileServerCanal);
			if (conexionFileServer) {

				log.info(mensajeTransaccion + " Se establecio conexion con el servidor : " + servidor);
				estadoEnvio = utilSFTP.enviarArchivoSftpFileServer(mensajeTransaccion, rutaOrigen, rutaDestino,
						archivoLocal);
			} else {
				log.info(mensajeTransaccion + "No se puede establecer conexion ");
			}

		} catch (Exception e) {
			log.error(Constantes.ERROR_DETALLE, e);
		}

		return estadoEnvio;
	}

	private List<String> obtenerArchivoFechVenc(String mensajeTransaccion,String ruta, String filtro) throws SFTPException {
		List<String> obtenerArchivo = null;
		boolean conexionFileServer = true;

		String rutaRemota = propertiesExternos.CONEXION_SFTP_BSCS_RUTA_ARCHIVO;
		String rutaDestino = ruta;
		String nombreArchivoRemoto = filtro;
		String extension = propertiesExternos.CONEXION_SFTP_BSCS_EXTENSION;
		String servidor = propertiesExternos.CONEXION_SFTP_BSCS_IP;
		String usuario = propertiesExternos.CONEXION_SFTP_BSCS_USUARIO;
		String password = propertiesExternos.CONEXION_SFTP_BSCS_CONTRASENA;
		int puerto = propertiesExternos.CONEXION_SFTP_BSCS_PUERTO;
		String timeout = propertiesExternos.CONEXION_SFTP_BSCS_TIMEOUT;

		try {

			conexionFileServer = utilSFTP.conectarSftpFileServer(mensajeTransaccion, servidor, usuario, password,
					puerto, timeout, propertiesExternos.pTIPO_CANAL_SESION);
			if (conexionFileServer) {
				log.info(mensajeTransaccion + " Se establecio conexion con el servidor : " + servidor);
				log.info(mensajeTransaccion + "Buscando el archivo : " + nombreArchivoRemoto + " en servidor : "
						+ servidor + " en la ruta " + rutaRemota);
				obtenerArchivo = utilSFTP.obtenerArchivoSFTP(mensajeTransaccion, extension, rutaRemota, rutaDestino,
						nombreArchivoRemoto, servidor);
				if (obtenerArchivo != null) {
					log.error(mensajeTransaccion + " Operacion exitosa");
					terminarConexiones(mensajeTransaccion);
				}
			}
		} catch (Exception e) {

			log.error(Constantes.ERROR_DETALLE, e);
		} finally {

			log.info(mensajeTransaccion + "FIN: [obtenerArchivoPlanoUsurio]");
		}
		return obtenerArchivo;
	}

	
	
	private List<String> obtenerArchivoPlanoUsurio(String mensajeTransaccion) throws SFTPException {
		List<String> obtenerArchivo = null;
		boolean conexionFileServer = true;

		String rutaRemota = propertiesExternos.CONEXION_SFTP_BSCS_RUTA_ARCHIVO;
		String rutaDestino = propertiesExternos.vRutaLocalEmisionRecibo;
		String nombreArchivoRemoto = propertiesExternos.CONEXION_SFTP_BSCS_ARCHIVO_BASE;
		String extension = propertiesExternos.CONEXION_SFTP_BSCS_EXTENSION;
		String servidor = propertiesExternos.CONEXION_SFTP_BSCS_IP;
		String usuario = propertiesExternos.CONEXION_SFTP_BSCS_USUARIO;
		String password = propertiesExternos.CONEXION_SFTP_BSCS_CONTRASENA;
		int puerto = propertiesExternos.CONEXION_SFTP_BSCS_PUERTO;
		String timeout = propertiesExternos.CONEXION_SFTP_BSCS_TIMEOUT;

		try {

			conexionFileServer = utilSFTP.conectarSftpFileServer(mensajeTransaccion, servidor, usuario, password,
					puerto, timeout, propertiesExternos.pTIPO_CANAL_SESION);
			if (conexionFileServer) {
				log.info(mensajeTransaccion + " Se establecio conexion con el servidor : " + servidor);
				log.info(mensajeTransaccion + "Buscando el archivo : " + nombreArchivoRemoto + " en servidor : "
						+ servidor + " en la ruta " + rutaRemota);
				obtenerArchivo = utilSFTP.obtenerArchivoSFTP(mensajeTransaccion, extension, rutaRemota, rutaDestino,
						nombreArchivoRemoto, servidor);
				if (obtenerArchivo != null) {
					log.error(mensajeTransaccion + " Operacion exitosa");
					terminarConexiones(mensajeTransaccion);
				}
			}
		} catch (Exception e) {

			log.error(Constantes.ERROR_DETALLE, e);
		} finally {

			log.info(mensajeTransaccion + "FIN: [obtenerArchivoPlanoUsurio]");
		}
		return obtenerArchivo;
	}

	public NotificarPlantillaRequest armarRequestNotificaRestFech(String archivo, String ubicacion, String Plantilla) {

		String host = propertiesExternos.FileServerIP;
		String user = propertiesExternos.FileServerUsuario;
		String pass = propertiesExternos.FileServerPass;
		String ruta = propertiesExternos.FileServerRutaRemota;

		NotificarPlantillaRequest notificRequest = new NotificarPlantillaRequest();
		Archivo objArchivo = new Archivo();
		objArchivo.setContrasena(pass);
		objArchivo.setIp(host);
		objArchivo.setUsuario(user);
		objArchivo.setRuta(ruta + archivo);

		String idPlantilla = Plantilla;
		notificRequest.setPlantilla(idPlantilla);
		notificRequest.setArchivo(objArchivo);
		notificRequest.setUbicacionRepositorio(ubicacion);
		return notificRequest;
	}

	public NotificarPlantillaRequest armarRequestNotificaRest(String archivo, String ubicacion,
			String PlantilalSinCargo, String PlantilalConCargo) {

		String host = propertiesExternos.FileServerIP;
		String user = propertiesExternos.FileServerUsuario;
		String pass = propertiesExternos.FileServerPass;
		String ruta = propertiesExternos.FileServerRutaRemota;

		NotificarPlantillaRequest notificRequest = new NotificarPlantillaRequest();
		Archivo objArchivo = new Archivo();
		objArchivo.setContrasena(pass);
		objArchivo.setIp(host);
		objArchivo.setUsuario(user);
		objArchivo.setRuta(ruta + archivo);

		String idPlantilla = utilitarios.obtenerIdPlantilla(archivo, PlantilalConCargo, PlantilalSinCargo);
		notificRequest.setPlantilla(idPlantilla);
		notificRequest.setArchivo(objArchivo);
		notificRequest.setUbicacionRepositorio(ubicacion);
		return notificRequest;
	}

	public void plantillaFechaVencimiento(String msTxT, String idTransaccion, String plantilla) {
		log.info(msTxT + " valor de plantilla :" + plantilla);
		log.info(msTxT+ "Actividad 2 - Inicio");
		try {
			log.info(msTxT + " [Actividad 2.1 - Depurar registros de la tabla temporal "+ Constantes.TABLA_FECH_VENC +" Inicio] ");
			RespuestaBean eliminarCargaFac = new RespuestaBean();

			eliminarCargaFac = bscsDao.eliminarFactVencidas(msTxT);

			if (String.valueOf(eliminarCargaFac.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_0)) {
				log.info(msTxT + Constantes.MENSAJE_EXITO + "[ : BD BSCS]");
			} else {
				log.info(msTxT + " Ocurrio un error al eliminar facturas vencidas ");

				return;
			}

			log.info(msTxT + " [Actividad 2.1 - Depurar registros de la tabla temporal "+ Constantes.TABLA_FECH_VENC +" Fin] ");

			String fechaVencimiento = utilitarios.formatoFechaReducida(propertiesExternos.dbOacdbFormatoFecha,
					propertiesExternos.dbOacdbRangoDiasVencmiento);
			
			String fechaProxima = Constantes.VACIO;
			
			log.info(msTxT + " [Actividad 2.2 - Lista Factura con fecha de Vencimiento - Inicio] ");
			ObtenerFechaVencimientoRequest fechaVencimientoRequest = new ObtenerFechaVencimientoRequest();
			fechaVencimientoRequest.setFechaVencimiento(fechaVencimiento);
			fechaVencimientoRequest.setFechaProxVencimiento(fechaProxima);

			ObtenerFechaVencimientoResponse fechaVencimientoResponse = new ObtenerFechaVencimientoResponse();
			fechaVencimientoResponse = oacDao.listarFechaVencimiento(msTxT, fechaVencimientoRequest);

			log.info(msTxT + " [Actividad 2.2 - Lista Factura con fecha de Vencimiento  - Fin] ");

			if (String.valueOf(fechaVencimientoResponse.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_0)) {

				log.info(msTxT + Constantes.MENSAJE_EXITO + "[ : BD BSCS]");
			} else {
				log.error(msTxT+"No se encontraron factuas vencidas");
				return;
			}

			log.info(msTxT + " [Actividad 2.3 -  Generar  base de Facturas con Fecha de Vencimiento a Notificar - Inicio]");
			List<CursorFechaVencimiento> listaFechaVencimiento = fechaVencimientoResponse.getListFechaVencimiento();

			int tamanoCursor = listaFechaVencimiento.size();

			log.info(msTxT + "Se obtuvo la lista con " + tamanoCursor + Constantes.ESPACIO + Constantes.REGISTROS);

			String[] filtroOrigen = propertiesExternos.vRutaLocalEmisionFiltro.split(Constantes.SEPARADOR);
			String filtroBSCS = filtroOrigen[Constantes.INT_CERO];
			String filtroSGA = filtroOrigen[Constantes.INT_UNO];

			List<String> listafacturasBSCS = new ArrayList<String>();
			List<String> listafacturasSGA = new ArrayList<String>();

			cargaFacturasVencidas(msTxT, listaFechaVencimiento, filtroBSCS, filtroSGA, listafacturasBSCS,
					listafacturasSGA);
			
			log.info(msTxT + "Se obtuvo la Facturas de BSCS con " + listafacturasBSCS.size() + Constantes.REGISTROS);
			log.info(msTxT + "Se obtuvo la Facturas de SGA con " + listafacturasSGA.size() + Constantes.REGISTROS);

			String rutaNombreArchivo = propertiesExternos.vRutaDataCtlFactura + propertiesExternos.FACVEN_NOMBRE_ARCHIVO
					+ propertiesExternos.LOCAL_EXTENSION_ARCHIVO;
			
			File archivoFechVencBSCS = utilitarios.generarArchivo(msTxT, rutaNombreArchivo, listafacturasBSCS);
			log.info(msTxT + " [Actividad 2.3 -  Generar  base de Facturas con Fecha de Vencimiento a Notificar - Fin]");
			
			log.info(msTxT + "[Actividad 2.4 - Registrar archivo plano en la tabla "+Constantes.TABLA_FECH_VENC+" Inicio]");
			
			String rutaArchivo = propertiesExternos.vRutaDataCtlFactura;
			
			String fechaCTL = utilitarios.formatoFecha(propertiesExternos.pFORMATO_FECHA_NOMBRE);
			
			String rutaCTL = utilitarios.armarRutaCTL(propertiesExternos.vRutaCtlUserApp,
					propertiesExternos.pNOMBRE_ARCHIVO_FACFECVEN_CTL, propertiesExternos.pEXTENSION_ARCHIVO_CTL);
			
			
			String rutaBad = utilitarios.armarSentenciaCTL(fechaCTL, propertiesExternos.vRutaBadCtlFactura,
					propertiesExternos.pNOMBRE_ARCHIVO_FACFECVEN_CTL, propertiesExternos.pEXTENSION_ARCHIVO_BAD);
			
			String rutaLOGCTL = utilitarios.armarSentenciaCTL(fechaCTL, propertiesExternos.vRutaLogCtlFactura,
					propertiesExternos.pNOMBRE_ARCHIVO_FACFECVEN_CTL, propertiesExternos.pEXTENSION_ARCHIVO_LOG);

			String rutaDiscard = utilitarios.armarSentenciaCTL(fechaCTL, propertiesExternos.vRutaDiscarCtlFactura,
					propertiesExternos.pNOMBRE_ARCHIVO_FACFECVEN_CTL, propertiesExternos.pEXTENSION_ARCHIVO_DSC);
			
			RespuestaBean cargaFacturaBSCS = new RespuestaBean();
			
			cargaFacturaBSCS = utilitarios.ejecutarSqlldr(msTxT, rutaBad, rutaLOGCTL, archivoFechVencBSCS, rutaCTL,
					rutaDiscard, propertiesExternos.userBSCSCTL, propertiesExternos.passBSCSCTL,
					propertiesExternos.dbBSCSCTL, propertiesExternos.perrorsCTL, propertiesExternos.pBindSizeCTL,
					propertiesExternos.pReadSizeCTL, propertiesExternos.prowsCTL);
			
			log.info(msTxT + "[Actividad 2.4 - Registrar archivo plano en la tabla "+Constantes.TABLA_FECH_VENC+" - Fin]");
			
			if (String.valueOf(cargaFacturaBSCS.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_0)) {

				log.info(msTxT + " Se cargo " + archivoFechVencBSCS + " en " + Constantes.TABLA_FECH_VENC);

			} else {
				log.error(msTxT + "ocurrio un error en la carga revisar log " + rutaLOGCTL);
			}
			
			log.info(msTxT + "[ Depurar Fichero temporal ]");
			
			utilitarios.eliminarFichero(msTxT, archivoFechVencBSCS);
			
			log.info(msTxT + "[Actividad 2.5 - Consultar Base de Facturas Vencidas a Notificar - Fin]");

			String fechaProximaVenc = Constantes.VACIO;
			
			RespuestaBean consultarFechVencResp=new RespuestaBean();
			
			consultarFechVencResp=bscsDao.consultarFechaVencimiento(msTxT, fechaProximaVenc);
			
			log.info(msTxT + "[Actividad 2.5 - Obtener Base de Facturas a Notifcar - Fin]");
			
			String filtroFechVenc = propertiesExternos.CONEXION_SFTP_BSCS_FECHVEN_BASE;
			
			
			List<String> listaArchivoFechVenc = new ArrayList<>();
			
			if (String.valueOf(consultarFechVencResp.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_0)) {
				log.info(msTxT + "[Actividad 2.6 - Extraer Fichero de Servidor de BSCS - Inicio]");
				listaArchivoFechVenc=obtenerArchivoFechVenc(msTxT, rutaArchivo, filtroFechVenc);
				log.info(msTxT + " Operacion exitosa");
				
				log.info(msTxT + "[Actividad 2.6 - Extraer Fichero de Servidor de BSCS - Fin]");
				
			}else {
					log.error(msTxT+ "No existen facturas a procesar");
					return;
				}
			
			log.info(msTxT + "Validando Peso archivo ");

			String rutaOrigen = propertiesExternos.vRutaLocalEmisionRecibo;
			String rutaEnvio = propertiesExternos.vRutaPendienteEnvioEmision;
			String extensionArchivo = propertiesExternos.vEmisionReciboExtension;
			
			enviarArchivoFecha(msTxT, idTransaccion, listaArchivoFechVenc, rutaOrigen, rutaEnvio, extensionArchivo, filtroFechVenc,plantilla);
			
		} catch (DBException e) {
			log.error(msTxT + "Error BD", e);
			log.info(msTxT + Constantes.CODIGO_ERROR + e.getCodError());
			log.info(msTxT + Constantes.MENSAJE_ERROR + e.getMsjError());
		} catch (SFTPException e) {
			log.error(msTxT + "Error SFTP", e);
			log.info(msTxT + Constantes.CODIGO_ERROR + e.getCodError());
			log.info(msTxT + Constantes.MENSAJE_ERROR + e.getMsjError());
		}  catch (WSException e) {
			log.error(msTxT + "Error WS", e);
			log.info(msTxT + Constantes.CODIGO_ERROR + e.getCodError());
			log.info(msTxT + Constantes.MENSAJE_ERROR + e.getMsjError());
		} 
		catch (Exception e) {
			log.info(msTxT + Constantes.MENSAJE_ERROR + e.getMessage());
		}

	}

	public void plantillaVencimientoProximo(String msTxT,String idTransaccion,String plantilla) {
		log.info(msTxT + " valor de plantilla :" + plantilla);
		log.info(msTxT+ "Actividad 3 - Inicio");
		try {
			log.info(msTxT + " [Actividad 3.1 - Depurar registros de la tabla temporal "+ Constantes.TABLA_FECH_VENC +" Inicio] ");
			RespuestaBean eliminarCargaFac = new RespuestaBean();

			eliminarCargaFac = bscsDao.eliminarFactVencidas(msTxT);

			if (String.valueOf(eliminarCargaFac.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_0)) {
				log.info(msTxT + Constantes.MENSAJE_EXITO + "[ : BD BSCS]");
			} else {
				log.info(msTxT + "Ocurrio un error al depurar los registros");
				return;
			}

			log.info(msTxT + " [Actividad 3.1 - Depurar registros de la tabla temporal "+ Constantes.TABLA_FECH_VENC +" Fin] ");
			
			String fechaVencimiento = utilitarios.formatoFechaReducida(propertiesExternos.dbOacdbFormatoFecha,
					propertiesExternos.dbOacdbRangoDiasResta);
			
			String fechaProxima = utilitarios.formatoFechaProxVenc(propertiesExternos.dbOacdbFormatoFecha,
					propertiesExternos.dbOacdbRangoDiasAdicion);
			
			log.info(msTxT + " [Actividad 3.2 - Listar Facturas  Proximo Vencimiento - Inicio] ");
			
			
			ObtenerFechaVencimientoRequest fechaVencimientoRequest = new ObtenerFechaVencimientoRequest();
			fechaVencimientoRequest.setFechaVencimiento(fechaVencimiento);
			fechaVencimientoRequest.setFechaProxVencimiento(fechaProxima);

			ObtenerFechaVencimientoResponse fechaVencimientoResponse = new ObtenerFechaVencimientoResponse();
			fechaVencimientoResponse = oacDao.listarFechaVencimiento(msTxT, fechaVencimientoRequest);
			
			
			log.info(msTxT + " [Actividad 3.2 - Listar Facturas  Proximo Vencimiento - Fin] ");

			if (String.valueOf(fechaVencimientoResponse.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_0)) {

				log.info(msTxT + Constantes.MENSAJE_EXITO + "[ : BD BSCS]");
			} else {
				log.error(msTxT+"No se encontraron factuas proximo vencimiento");
				return;
			}
			
			log.info(msTxT+ " [Actividad 3.3 - Generar  base de Facturas con proximo  de Vencimiento - Inicio]");
			List<CursorFechaVencimiento> listaFechaVencimiento = fechaVencimientoResponse.getListFechaVencimiento();
			
			int tamanoCursor = listaFechaVencimiento.size();

			log.info(msTxT + "Se obtuvo la lista con " + tamanoCursor + Constantes.ESPACIO + Constantes.REGISTROS);
			
			String[] filtroOrigen = propertiesExternos.vRutaLocalEmisionFiltro.split(Constantes.SEPARADOR);
			String filtroBSCS = filtroOrigen[Constantes.INT_CERO];
			String filtroSGA = filtroOrigen[Constantes.INT_UNO];

			List<String> listafacturasBSCS = new ArrayList<String>();
			List<String> listafacturasSGA = new ArrayList<String>();
			
			cargaFacturasVencidas(msTxT, listaFechaVencimiento, filtroBSCS, filtroSGA, listafacturasBSCS,
					listafacturasSGA);
			
			log.info(msTxT + "Se obtuvo la Facturas de BSCS con " + listafacturasBSCS.size() + Constantes.REGISTROS);
			log.info(msTxT + "Se obtuvo la Facturas de SGA con " + listafacturasSGA.size() + Constantes.REGISTROS);
			
			String rutaNombreArchivo = propertiesExternos.vRutaDataCtlFactura + propertiesExternos.FACVEN_NOMBRE_ARCHIVO
					+ propertiesExternos.LOCAL_EXTENSION_ARCHIVO;
			
			File archivoFechVencBSCS = utilitarios.generarArchivo(msTxT, rutaNombreArchivo, listafacturasBSCS);
			log.info(msTxT+ " [Actividad 3.3 - Generar  base de Facturas con proximo  de Vencimiento - Fin]");
			
			log.info(msTxT + "[Actividad 3.4 - Registrar archivo plano en la tabla "+Constantes.TABLA_FECH_VENC+" Inicio]");
			
			String rutaArchivo = propertiesExternos.vRutaDataCtlFactura;
			
			String fechaCTL = utilitarios.formatoFecha(propertiesExternos.pFORMATO_FECHA_NOMBRE);
			
			String rutaCTL = utilitarios.armarRutaCTL(propertiesExternos.vRutaCtlUserApp,
					propertiesExternos.pNOMBRE_ARCHIVO_FACFECVEN_CTL, propertiesExternos.pEXTENSION_ARCHIVO_CTL);
			
			
			String rutaBad = utilitarios.armarSentenciaCTL(fechaCTL, propertiesExternos.vRutaBadCtlFactura,
					propertiesExternos.pNOMBRE_ARCHIVO_FACFECVEN_CTL, propertiesExternos.pEXTENSION_ARCHIVO_BAD);
			
			String rutaLOGCTL = utilitarios.armarSentenciaCTL(fechaCTL, propertiesExternos.vRutaLogCtlFactura,
					propertiesExternos.pNOMBRE_ARCHIVO_FACFECVEN_CTL, propertiesExternos.pEXTENSION_ARCHIVO_LOG);

			String rutaDiscard = utilitarios.armarSentenciaCTL(fechaCTL, propertiesExternos.vRutaDiscarCtlFactura,
					propertiesExternos.pNOMBRE_ARCHIVO_FACFECVEN_CTL, propertiesExternos.pEXTENSION_ARCHIVO_DSC);
			
			RespuestaBean cargaFacturaBSCS = new RespuestaBean();
			
			cargaFacturaBSCS = utilitarios.ejecutarSqlldr(msTxT, rutaBad, rutaLOGCTL, archivoFechVencBSCS, rutaCTL,
					rutaDiscard, propertiesExternos.userBSCSCTL, propertiesExternos.passBSCSCTL,
					propertiesExternos.dbBSCSCTL, propertiesExternos.perrorsCTL, propertiesExternos.pBindSizeCTL,
					propertiesExternos.pReadSizeCTL, propertiesExternos.prowsCTL);
			
			log.info(msTxT + "[Actividad 3.4 - Registrar archivo plano en la tabla "+Constantes.TABLA_FECH_VENC+" Inicio]");
			
			if (String.valueOf(cargaFacturaBSCS.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_0)|| 
					String.valueOf(cargaFacturaBSCS.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_2)) {

				log.info(msTxT + " Se cargo " + archivoFechVencBSCS + " en " + Constantes.TABLA_FECH_VENC);

			} else {
				log.error(msTxT + "ocurrio un error en la carga revisar log " + rutaLOGCTL);
				return;
			}
			
			log.info(msTxT + "[ Depurar Fichero temporal ]");
			
			utilitarios.eliminarFichero(msTxT, archivoFechVencBSCS);

			
			log.info(msTxT + "[Actividad 3.5 - Consultar  Base de Facturas Vencidas a Notificar - Inicio]");
			
			
			RespuestaBean consultarFechVencResp=new RespuestaBean();
			
			consultarFechVencResp=bscsDao.consultarFechaProxVencimiento(msTxT, fechaProxima);
			
			log.info(msTxT + "[Actividad 3.5 - Consultar  Base de Facturas Vencidas a Notificar - Inicio]");
			
			
			String filtroFechVenc=propertiesExternos.CONEXION_SFTP_BSCS_FECHPROXVEN_BASE;
			
			List<String> listaArchivoFechVenc = new ArrayList<>();
			
			if (String.valueOf(consultarFechVencResp.getCodRespuesta()).equals(Constantes.CODIGO_EXITO_0)) {
				
				
				log.info(msTxT + "[Actividad 3.6 - Extraer Fichero de Servidor de BSCS - Inicio]");
				listaArchivoFechVenc=obtenerArchivoFechVenc(msTxT, rutaArchivo, filtroFechVenc);
				log.info(msTxT + " Operacion exitosa");
				
				log.info(msTxT + "[Actividad 3.6 - Extraer Fichero de Servidor de BSCS - Fin]");
				
			}else {
					log.error(msTxT+ "No existen facturas a procesar");
					return;
				}
			
			log.info(msTxT + "Validando Peso archivo ");
			
			String rutaOrigen = propertiesExternos.vRutaLocalEmisionRecibo;
			String rutaEnvio = propertiesExternos.vRutaPendienteEnvioEmision;
			String extensionArchivo = propertiesExternos.vEmisionReciboExtension;
			
			enviarArchivoFecha(msTxT, idTransaccion, listaArchivoFechVenc, rutaOrigen, rutaEnvio, extensionArchivo, filtroFechVenc,plantilla);
			
		} catch (DBException e) {
			log.error(msTxT + "Error BD", e);
			log.info(msTxT + Constantes.CODIGO_ERROR + e.getCodError());
			log.info(msTxT + Constantes.MENSAJE_ERROR + e.getMsjError());
		} catch (SFTPException e) {
			log.error(msTxT + "Error SFTP", e);
			log.info(msTxT + Constantes.CODIGO_ERROR + e.getCodError());
			log.info(msTxT + Constantes.MENSAJE_ERROR + e.getMsjError());
		}  catch (WSException e) {
			log.error(msTxT + "Error WS", e);
			log.info(msTxT + Constantes.CODIGO_ERROR + e.getCodError());
			log.info(msTxT + Constantes.MENSAJE_ERROR + e.getMsjError());
		} 
		catch (Exception e) {
			log.info(msTxT + Constantes.MENSAJE_ERROR + e.getMessage());
		}
		
		
	}
}
