package pe.com.claro.sh.programainformativa.factura.bean;

public class ObtenerFacturasEmitidasRequest {

	private String fechaEmision;

	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

}
