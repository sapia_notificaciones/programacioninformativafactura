package pe.com.claro.sh.programainformativa.factura.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.NestedRuntimeException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.com.claro.sh.programainformativa.factura.bean.CursorCampanaNotifcar;
import pe.com.claro.sh.programainformativa.factura.bean.ObtieneCampanaActivaRequest;
import pe.com.claro.sh.programainformativa.factura.bean.ObtieneCampanaActivaResponse;
import pe.com.claro.sh.programainformativa.factura.exception.DBException;
import pe.com.claro.sh.programainformativa.factura.util.Constantes;
import pe.com.claro.sh.programainformativa.factura.util.PropertiesExternos;
import pe.com.claro.sh.programainformativa.factura.util.Util;

@Repository
public class NotifDaoImpl implements NotifDao {

	private static transient Logger logger = Logger.getLogger(NotifDaoImpl.class);

	@Autowired
	private PropertiesExternos propertiesExterno;

	@Autowired
	@Qualifier("notifdbDS")
	private DataSource notifdbDS;

	@SuppressWarnings({ "unchecked" })
	@Override
	public ObtieneCampanaActivaResponse buscarCampanaActiva(String msjTransaccion, ObtieneCampanaActivaRequest request)
			throws DBException {

		String msjTx = msjTransaccion + "[buscarCampanaActiva]";
		ObtieneCampanaActivaResponse responseBean = new ObtieneCampanaActivaResponse();
		List<CursorCampanaNotifcar> lista = new ArrayList<>();

		try {
			logger.info(msjTx + Constantes.METODO_DAO_MENSAJE_CONSULTANDO_BD + propertiesExterno.dbNotifdb + "]");
			SimpleJdbcCall procConsulta1 = new SimpleJdbcCall(notifdbDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbNotifdbOwner)
					.withCatalogName(propertiesExterno.dbNotifdbPkgOacNotifInfo)
					.withProcedureName(propertiesExterno.dbNotifdbspCampanaActiva)
					.declareParameters(new SqlParameter(Constantes.K_PLANTILLA, OracleTypes.VARCHAR),
							new SqlOutParameter(Constantes.K_NOTIFICACIONES, OracleTypes.CURSOR,
									new RowMapper<CursorCampanaNotifcar>() {

										@Override
										public CursorCampanaNotifcar mapRow(ResultSet rs, int arg1)
												throws SQLException {
											CursorCampanaNotifcar bean = new CursorCampanaNotifcar();
											bean = llenaCursor(rs);
											return bean;
										}
									}),
							new SqlOutParameter(Constantes.K_MSJ_RESULTADO, OracleTypes.VARCHAR),
							new SqlOutParameter(Constantes.K_COD_RESULTADO, OracleTypes.VARCHAR));

			logger.info(msjTx + Constantes.METODO_DAO_MENSAJE_SE_INVOCARA_SP + propertiesExterno.dbNotifdbOwner
					+ Constantes.STRING_PUNTO + propertiesExterno.dbNotifdbPkgOacNotifInfo + Constantes.STRING_PUNTO
					+ propertiesExterno.dbNotifdbspCampanaActiva);
			logger.info(msjTx + "PARAMETROS [INPUT]: ");
			logger.info(msjTx + "[PI_CODIGO_PLANTILLA] = " + request.getCodigoPlatntilla());

			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					.addValue(Constantes.K_PLANTILLA, request.getCodigoPlatntilla());

			procConsulta1.getJdbcTemplate()
					.setQueryTimeout(Integer.parseInt(propertiesExterno.dbNotifdbTimeOutExecutionMaxTime));

			Map<String, Object> resultMap = procConsulta1.execute(objParametrosIN);

			logger.info(msjTx + "Se invoco con exito el SP: " + propertiesExterno.dbNotifdbOwner + Constantes.STRING_PUNTO
					+ propertiesExterno.dbNotifdbPkgOacNotifInfo + Constantes.STRING_PUNTO
					+ propertiesExterno.dbNotifdbspCampanaActiva);

			responseBean.setCodRespuesta((String) resultMap.get(Constantes.K_MSJ_RESULTADO));
			responseBean.setMsjRespuesta((String) resultMap.get(Constantes.K_COD_RESULTADO));

			lista = (List<CursorCampanaNotifcar>) resultMap.get(Constantes.K_NOTIFICACIONES);

			if (lista != null && !lista.isEmpty()) {
				for (CursorCampanaNotifcar cursorCobranza : lista) {
					logger.info(msjTx + "Resultado CURSOR: " + cursorCobranza.toString());
				}
				responseBean.setCursorCampanaNotifca(lista);
			} else {
				logger.info(msjTx + "Lista Vacia");
			}

			logger.info(msjTx + Constantes.METODO_DAO_MENSAJE_PARAMETROS_OUTPUT);
			logger.info(msjTx + "[PO_MSJRPTA] = " + responseBean.getMsjRespuesta());
			logger.info(msjTx + "[PO_CODRPTA] = " + responseBean.getCodRespuesta());
		} catch (NestedRuntimeException e) {
			String textoError = Util.getStackTraceFromException(e);
			logger.error(msjTx + Constantes.METODO_DAO_MENSAJE_NESTEDRUNTIMEEXCEPTION + textoError, e);

			String mensajeError = "";
			if (e.contains(SQLTimeoutException.class)) {
				mensajeError = propertiesExterno.spIdt2Mensaje.replace(Constantes.BD_CON_CORCHETE,
						propertiesExterno.dbNotifdb);
				mensajeError += e.getMessage();
				throw new DBException(propertiesExterno.spIdt2Codigo, mensajeError, e);
			} else {
				mensajeError = propertiesExterno.spIdt1Mensaje.replace(Constantes.BD_CON_CORCHETE,
						propertiesExterno.dbNotifdb);
				mensajeError += Constantes.SP_MENSAJE_ERROR_EJECUTANDO + "["
						+ propertiesExterno.dbNotifdbPkgOacNotifInfo + Constantes.STRING_PUNTO
						+ propertiesExterno.dbNotifdbspCampanaActiva + "] " + e.getMessage();
				throw new DBException(propertiesExterno.spIdt1Codigo, mensajeError, e);
			}
		} catch (Exception e) {
			String textoError = Util.getStackTraceFromException(e);
			logger.error(msjTx + Constantes.METODO_DAO_MENSAJE_EXCEPTION + textoError, e);

			String mensajeError = "";
			mensajeError = propertiesExterno.spIdt1Mensaje.replace(Constantes.BD_CON_CORCHETE,
					propertiesExterno.dbNotifdb);
			mensajeError += Constantes.SP_MENSAJE_ERROR_EJECUTANDO + "[" + propertiesExterno.dbNotifdbPkgOacNotifInfo
					+ Constantes.STRING_PUNTO + propertiesExterno.dbNotifdbspCampanaActiva + "] " + e.getMessage();
			throw new DBException(propertiesExterno.spIdt1Codigo, mensajeError, e);
		}

		return responseBean;

	}
	
	
	
	private CursorCampanaNotifcar llenaCursor(ResultSet rs) throws SQLException {
		CursorCampanaNotifcar bean = new CursorCampanaNotifcar();

		bean.setCodCanal(rs.getString(Constantes.CURSOR_SP_EAISS_BUSCAR_CAMP_ACTIVO_CODTPL));
		bean.setTipoNotificacion(rs.getString(Constantes.CURSOR_SP_EAISS_BUSCAR_CAMP_ACTIVO_TIPO));
		bean.setCodCanal(rs.getString(Constantes.CURSOR_SP_EAISS_BUSCAR_CAMP_ACTIVO_CODCANAL));

		return bean;
	}
}
