package pe.com.claro.sh.programainformativa.factura.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import pe.com.claro.sh.programainformativa.factura.bean.CursorFechaVencimiento;
import pe.com.claro.sh.programainformativa.factura.bean.RespuestaBean;

@Component
public class Utilitarios {

	

	private static Logger log = Logger.getLogger(Utilitarios.class);

	public static long obtenerTiempoEnMiliSegundos(){
		return System.currentTimeMillis();
	}
	
	public ArrayList<String> generarBaseFacturas(String msjtTransaccion, File archivo, String separador, int posicion,
			String filtroOrigen, int valorAdicionar) {
		long ini = System.currentTimeMillis();
		log.info(msjtTransaccion + "[generarBaseFacturas]");

		ArrayList<String> listaFacturas = new ArrayList<String>();

		try (BufferedReader buffer = new BufferedReader(new FileReader(archivo))) {

			String strLine = "";
			while ((strLine = buffer.readLine()) != null) {
				validarExistenArchivo(msjtTransaccion, archivo, separador, posicion, filtroOrigen, valorAdicionar,
						listaFacturas, strLine);
			}

		} catch (Exception e) {


			log.error(Constantes.ERROR_DETALLE ,e);
		} finally {

			log.info("Tiempo bytes[] " + (System.currentTimeMillis() - ini));
		}

		return listaFacturas;

	}

	private void validarExistenArchivo(String msjtTransaccion, File archivo, String separador, int posicion,
			String filtroOrigen, int valorAdicionar, ArrayList<String> listaFacturas, String strLine) {
		if (!strLine.isEmpty()) {
			String[] arregloFacturas = strLine.split(separador);
			if (arregloFacturas[posicion].equals(filtroOrigen)) {
				listaFacturas.add(arregloFacturas[valorAdicionar]);
			}

		} else {
			log.info(msjtTransaccion + "Archivo " + archivo + "no contiene registros");
		}
	}

	
	
	
	public String generarAleatorio(int valorMaximo) {

		Random num = new Random();

		String idTransaccionRandon = String.format(Constantes.FORMATO_RANDON, num.nextInt(valorMaximo));


		return idTransaccionRandon;
	}
	
	public void generarArchFechaVenc(String msjTx, String nombreArchivo,List<CursorFechaVencimiento> listaFechaVenc){
		log.info("[INICIO][Metodo: generarArchFechaVenc]");
		File archivoFechVenc=new File(nombreArchivo);
	
		int buffer=Constantes.BUFFER*Constantes.MEGA;
		
		try (Writer writer = new BufferedWriter(new FileWriter(archivoFechVenc), buffer)){
			StringBuilder stringBuilder;
			stringBuilder = new StringBuilder();
			for (Iterator iterador=listaFechaVenc.iterator() ;iterador.hasNext(); ) {
				
				CursorFechaVencimiento fechaVenc=(CursorFechaVencimiento)iterador.next();
				stringBuilder.append(fechaVenc.getFactura().concat(Constantes.PALOTE));
				
				stringBuilder.append(fechaVenc.getOrigen().concat(Constantes.PALOTE));
				stringBuilder.append(Constantes.SALTO_LINEASP);
				writer.write(stringBuilder.toString());
				stringBuilder = new StringBuilder();
			}
			
			
		} catch (IOException e) {
			log.error("[ERROR][" + e + "]");
		}
	
		
	}

	public File generarArchivo(String msjTx, String nombreRutaArchivo, List<String> contenidoArchivo) {
		log.info("[INICIO][Metodo: generarArchivo]");
		File nuevoArchivo = new File(nombreRutaArchivo);

		log.info(msjTx + "total de registros : " + contenidoArchivo.size());

		int buffer = Constantes.BUFFER * Constantes.MEGA;
		int cantRegistrosvacios = 0, cantRegistrosLLenos = 0;
		try (Writer writer = new BufferedWriter(new FileWriter(nuevoArchivo), buffer))
		{
		
				for (int i=0;i<contenidoArchivo.size();i++) {

					String strLine = "";
					if (contenidoArchivo.get(i) != null && !contenidoArchivo.get(i).isEmpty()) {
						strLine = contenidoArchivo.get(i);
						cantRegistrosLLenos++;
						writer.write(strLine.trim() + "\r\n");

					} else {
						cantRegistrosvacios++;

					}
				}

			

			
		} catch (IOException e) {
			log.error("[ERROR][" + e + "]");
		} finally {
			log.info(msjTx + " Total de registros con valores vacios : " + cantRegistrosvacios);
			log.info(msjTx + " Total de registros con valores llenos : " + cantRegistrosLLenos);
		}

		return nuevoArchivo;

	}

	public RespuestaBean ejecutarSqlldr(String mensajeTransaccion, String rutaBAD, String rutaLOG, File rutaDATA,
			String rutaCTL, String rutaDiscard, String usuario, String contrasenia, String BD, int error, int bindsize,
			int readsize, int rows) throws IOException {

		long tiempoInicial = Utilitarios.obtenerTiempoEnMiliSegundos();

		String metodo = "ejecutarSqlldr";
		String cadMensaje = mensajeTransaccion + "[" + metodo + "] ";

		log.info(cadMensaje + "- Inicio del metodo ejecutar Sqlldr");

		RespuestaBean response = new RespuestaBean();

		BigDecimal codigoRespuestaExito = new BigDecimal(Constantes.INT_CERO);
		BigDecimal codigoRespuestaError = new BigDecimal(Constantes.INT_UNO);
		
		StringBuilder comandoSQL = new StringBuilder();
		comandoSQL.append(Constantes.CTL_SQLLDR).append(Constantes.ESPACIO_BLANCO)
		.append(Constantes.CTL_USER_ID).append(usuario).append(Constantes.SLASH).append(contrasenia)
		.append(Constantes.ARROBA).append(BD).append(Constantes.ESPACIO_BLANCO)
		.append(Constantes.SIGNO_REPLACE_COMA).append(Constantes.CTL_CONTROL).append(rutaCTL)
		.append(Constantes.ESPACIO_BLANCO).append(Constantes.SIGNO_REPLACE_COMA)
		.append(Constantes.CTL_DATA).append(rutaDATA).append(Constantes.ESPACIO_BLANCO)
		.append(Constantes.SIGNO_REPLACE_COMA).append(Constantes.CTL_LOG).append(rutaLOG)
		.append(Constantes.ESPACIO_BLANCO).append(Constantes.SIGNO_REPLACE_COMA)
		.append(Constantes.CTL_BAD).append(rutaBAD).append(Constantes.ESPACIO_BLANCO)
		.append(Constantes.SIGNO_REPLACE_COMA).append(Constantes.CTL_DISCARD).append(rutaDiscard)
		.append(Constantes.ESPACIO_BLANCO).append(Constantes.SIGNO_REPLACE_COMA)
		.append(Constantes.CTL_BINDSIZE).append(bindsize).append(Constantes.ESPACIO_BLANCO)
		.append(Constantes.SIGNO_REPLACE_COMA).append(Constantes.CTL_READSIZE).append(readsize)
		.append(Constantes.ESPACIO_BLANCO).append(Constantes.SIGNO_REPLACE_COMA)
		.append(Constantes.CTL_ROWS).append(rows).append(Constantes.ESPACIO_BLANCO)
		.append(Constantes.SIGNO_REPLACE_COMA).append(Constantes.CTL_ERROS).append(error);
		Process proceso = Runtime.getRuntime().exec(comandoSQL.toString());
		try (BufferedReader stdInput = new BufferedReader(new InputStreamReader(proceso.getInputStream()))){

			

				log.info(mensajeTransaccion + "- Ejecutando Sqlldr  en " + BD);

				
				log.info(mensajeTransaccion + "comando sqlldr : " + comandoSQL.toString());

				int respuestaProceso = proceso.waitFor();
				

				BufferedReader stdError = new BufferedReader(new InputStreamReader(proceso.getErrorStream()));

				String cantidadLineas = null;
				while ((cantidadLineas = stdInput.readLine()) != null) {
					log.info(cadMensaje + "Log de Ejecucion CTL :" + cantidadLineas);
				}

				while ((cantidadLineas = stdError.readLine()) != null) {
					log.info(cadMensaje + " Log de Ejecucion CTL  :" + cantidadLineas);
				}

				if (respuestaProceso == 0 || respuestaProceso == 2) {
					log.info(cadMensaje + "Carga ha sido completada");

					log.info(cadMensaje + "Respuesta de carga : " + respuestaProceso);

					response.setCodRespuesta(codigoRespuestaExito);

				} else {

					log.info(cadMensaje + "Respuesta de carga : " + respuestaProceso);
					response.setCodRespuesta(codigoRespuestaError);

				}
		

			

		} catch (Exception e) {
			
			log.error(mensajeTransaccion + e);
		} finally {

			tiempoEjecucion(tiempoInicial, cadMensaje);
			log.info(cadMensaje + " ["+ metodo + "] FIN");
		}

		return response;
	}

	private void tiempoEjecucion(long tiempoInicial, String cadMensaje) {
		log.info(cadMensaje + Constantes.TIEMPO_EJECUCION + (System.currentTimeMillis() - tiempoInicial)
				+ " ms]");
	}
	
	public static String aString(Date fechaDate, String formato) {
		String fechaString = null;

		if (fechaDate != null) {
			SimpleDateFormat sdf = new SimpleDateFormat(formato,
					Locale.getDefault());
			try {
				fechaString = sdf.format(fechaDate);

			} catch (Exception e) {
				log.error("ERROR [Exception]: " + e.getMessage(), e);
			}

		}

		return fechaString;
	}
	
	public static String aString( Date fechaDate) {
		String fechaString = null;

		if (fechaDate != null) {
			fechaString = aString(fechaDate, Constantes.FECHA_OAC);
		}

		return fechaString;
	}

	public String formatearFecha(String msjTxIn,  String formato) {
        
        String fecha = null;
        SimpleDateFormat sdf = new SimpleDateFormat(formato,
                Locale.getDefault());
        try {
            fecha = sdf.format(new Date());

        } catch (Exception e) {
            log.error(msjTxIn + "ERROR [Exception]: " + e.getMessage(), e);
        } 
        
        return fecha;
    }
	
	public String formatoFecha(String formatoFecha) {
		String fechaString = null;

		DateTimeFormatter formatoFechaOAC = DateTimeFormatter.ofPattern(formatoFecha);
		fechaString = LocalDate.now().format(formatoFechaOAC);

		return fechaString;

	}

	public String formatoFechaProxVenc(String formatoFecha, int cantidad) {
		String fechaString = null;

		DateTimeFormatter formatFecha = DateTimeFormatter.ofPattern(formatoFecha);
		fechaString = LocalDate.now().plusDays(cantidad).format(formatFecha);

		return fechaString;

	}
	public String formatoFechaReducida(String formatoFecha, int cantidad) {
		String fechaString = null;
		
		DateTimeFormatter formatFecha = DateTimeFormatter.ofPattern(formatoFecha);
		fechaString = LocalDate.now().minusDays(cantidad).format(formatFecha);
		
		return fechaString;
		
	}

	public String armarSentenciaCTL(String fechaCTL, String ruta, String nombreArchivo, String extension) {
		StringBuilder rutaBad = new StringBuilder();
		rutaBad.append(ruta);
		rutaBad.append(nombreArchivo);
		rutaBad.append(Constantes.GUION_ABAJO);
		rutaBad.append(fechaCTL);
		rutaBad.append(extension);
		return rutaBad.toString();
	}

	public String armarRutaCTL(String ruta, String nombre, String extension) {

		StringBuilder rutaCTL = new StringBuilder();
		rutaCTL.append(ruta);
		rutaCTL.append(nombre);
		rutaCTL.append(extension);
		return rutaCTL.toString();
	}

	public double obtenerPesoArchivoMega(String transaccion, File fichero) {
		double megabytes = 0;
		
		if (fichero.exists()) {
			double bytes = fichero.length();

			double kilobytes = (bytes / Constantes.MEGA);
			megabytes = (kilobytes / Constantes.MEGA);
			
			
			log.info(transaccion + "archivo : "+fichero+" pesa "+ megabytes + " MB");

		} else {
			log.info(transaccion + "No existe archivo ");
		}

		return megabytes;
	}

	
	public int obtenerCantidadArchivo(long pesoMaximoFichero, long pesoFicheroActual){
		int cantidadArchivos=0;
		cantidadArchivos=Math.round(pesoFicheroActual/pesoMaximoFichero)+1;
		return cantidadArchivos;
	}
	

	public long obtenerNumeroRegistros(String trazabilidadParam, String rutaFichero, String nombreFichero) {
		long registros = 0;
		try {
			StringBuilder comandoObtenerNumeroRegistros = new StringBuilder();
			comandoObtenerNumeroRegistros.append(Constantes.COMANDO_MOSTRAR).append(Constantes.ESPACIO_BLANCO)
					.append("$(").append(Constantes.COMANDO_CD).append(Constantes.ESPACIO_BLANCO).append(rutaFichero)
					.append(Constantes.ESPACIO_BLANCO).append(Constantes.PUNTO_COMA).append(Constantes.ESPACIO_BLANCO)
					.append(Constantes.COMND_CONTAR_LINEAS).append(Constantes.ESPACIO_BLANCO).append(nombreFichero)
					.append(")").append(Constantes.ESPACIO_BLANCO).append(Constantes.PALOTE)
					.append(Constantes.ESPACIO_BLANCO).append(Constantes.COMANDO_AWK).append(Constantes.COMILLA)
					.append(Constantes.COMANDO_AWK_BEGIN).append(Constantes.ESPACIO_BLANCO)
					.append(Constantes.COMILLA_DOBLE).append(Constantes.ESPACIO_BLANCO).append(Constantes.COMILLA_DOBLE)
					.append("}").append(Constantes.ESPACIO_BLANCO).append("{print $1}").append(Constantes.COMILLA);

			log.info(
					trazabilidadParam + "comando obtenerNumeroRegistros : " + comandoObtenerNumeroRegistros.toString());

			Process proceso = Runtime.getRuntime().exec(new String[] { Constantes.COMANDO_SH, Constantes.COMANDO_C,
					comandoObtenerNumeroRegistros.toString() });

			int respuestaProceso = proceso.waitFor();

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proceso.getInputStream()));

			String cantidadLineas = stdInput.readLine();

			log.info(trazabilidadParam + "respuesta de obtener Numero de Registros : " + cantidadLineas);


			if (respuestaProceso == 0 || respuestaProceso == 2) {
				log.info(trazabilidadParam + " Ejecucion con exito");

				registros= Long.parseLong(cantidadLineas);
			} else {

				log.info(trazabilidadParam + " Ocurrio un error al obtener el numero de lineas : " + respuestaProceso);
				return 0;
			}

		} catch (Exception e) {
			log.error(trazabilidadParam + Constantes.ERROR_DETALLE, e);
			registros= 0;
		}
		return registros;

	}

	public RespuestaBean dividirArchivo(String transaccion, String ruta, String archivoBase, String archivoNuevo,
			String extensionArchivo, Long tamano) {

		RespuestaBean dividirArchivo = new RespuestaBean();

		BigDecimal codigoRespuestaExito = new BigDecimal(Constantes.CODIGO_EXITO_0);
		BigDecimal codigoRespuestaError = new BigDecimal(Constantes.CODIGO_ERROR_1);

		try {

			StringBuilder comandodividirArchivo = new StringBuilder();
			comandodividirArchivo.append(Constantes.COMANDO_CD).append(Constantes.ESPACIO_BLANCO).append(ruta)
					.append(Constantes.ESPACIO_BLANCO).append(Constantes.EJECUCION_SECUENCIA)
					.append(Constantes.ESPACIO_BLANCO).append(Constantes.COMANDO_AWK_LINEAS).append(tamano)
					.append("==1{").append("x=").append(Constantes.COMILLA_DOBLE).append(archivoNuevo)
					.append(Constantes.COMILLA_DOBLE).append("++i").append(Constantes.COMILLA_DOBLE)
					.append(extensionArchivo).append(Constantes.COMILLA_DOBLE).append(Constantes.PUNTO_COMA)
					.append("}{print > x}").append(Constantes.COMILLA).append(Constantes.ESPACIO_BLANCO)
					.append(archivoBase);

			log.info(transaccion + " comando dividirArchivo : " + comandodividirArchivo.toString());

			Process proceso = Runtime.getRuntime().exec(
					new String[] { Constantes.COMANDO_SH, Constantes.COMANDO_C, comandodividirArchivo.toString() });

			int respuestaProceso = proceso.waitFor();
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proceso.getInputStream()));

			String cantidadLineas = null;
			while ((cantidadLineas = stdInput.readLine()) != null) {
				log.info(transaccion + "Log de Ejecucion dividirArchivo :" + cantidadLineas);

			}

			if (respuestaProceso == 0 || respuestaProceso == 2) {
				log.info(transaccion + "Operacion exito de dividirArchivo");

				log.info(transaccion + "Respuesta de metodo : " + respuestaProceso);

				dividirArchivo.setCodRespuesta(codigoRespuestaExito);

			} else {

				log.info(transaccion + "Respuesta de metodo dividirArchivo : " + respuestaProceso);
				dividirArchivo.setCodRespuesta(codigoRespuestaError);

			}
		} catch (IOException e) {
			
			log.error(Constantes.ERROR_DETALLE ,e);
		} catch (InterruptedException e) {
			
			log.error(Constantes.ERROR_DETALLE,e);

		}

		return dividirArchivo;

	}

	public void eliminarFichero(String mensajeTransaccion, File folder) {

		String metodo = "eliminarFichero";
		String cadMensaje = mensajeTransaccion + "[" + metodo + "] ";
		log.info(cadMensaje + "[INICIO] - METODO: [" + metodo + "]");

		String ruta=folder.getAbsolutePath().toString().substring(Constantes.INT_CERO,folder.getAbsolutePath().toString().lastIndexOf(File.separator));
		
		Long ini = 0L;
		Long end = 0L;

		ini = System.currentTimeMillis();
		try {

			log.info(cadMensaje + "El archivo:" + folder.getName() + "  fue eliminado del directorio : " + ruta);
			folder.delete();


		} catch (Exception e) {
			
			log.error(Constantes.ERROR_DETALLE,e);
		} finally {
			end = System.currentTimeMillis();
			log.info(cadMensaje + Constantes.TIEMPO_EJECUCION + (end - ini) + " ms]");
			log.info(cadMensaje + "[FIN] - METODO: [" + metodo + "]");
		}

		

	}

	public boolean eliminarContenidoFolder(String mensajeTransaccion, File folder) {

		boolean exito = false;
		String metodo = "eliminarContenidoFolder";
		String cadMensaje = mensajeTransaccion + "[" + metodo + "] ";
		log.info(cadMensaje + "[INICIO] - METODO: [" + metodo + " - UTILITARIOS] ");

		Long ini = Utilitarios.obtenerTiempoEnMiliSegundos();

		
		try {
			String[] entries = folder.list();

			if (!folder.exists()) {
				exito = false;
				log.info(cadMensaje + "La ruta no existe.");
			} else {
				for (String s : entries) {
					File currentFile = new File(folder.getPath(), s);

					currentFile.delete();
					log.info(cadMensaje + "El archivo:" + currentFile.getName() + "  fue eliminado.");
				}
				exito = true;

			}

		} catch (Exception e) {
		
			log.info(Constantes.ERROR_DETALLE,e);
		} finally {
			log.info(cadMensaje + Constantes.TIEMPO_EJECUCION+ (System.currentTimeMillis() - ini) + " ms]");
			log.info(cadMensaje + "[FIN] - METODO: [" + metodo + " - UTILITARIOS] ");
		}

		return exito;

	}

	public List<String> listarArchivos(String mensajeTransaccion, String directorio, String filtro) {
		File directorioFichero = new File(directorio);
		List<String> listaArchivos = new ArrayList<>();
		if (directorioFichero.isDirectory()) {
			
			for (File fichero : directorioFichero.listFiles()) {
				if (fichero.getName().toString().contains(filtro)) {
					listaArchivos.add(fichero.getName());

				} else {
					log.info(mensajeTransaccion + fichero.getName()  + " no cumple con el filtro"+ filtro);
				}
			}
		} else{
			log.info(mensajeTransaccion+ directorio+" ruta no es directorio");
		}
		
		

		return listaArchivos;
	}

	public void moverArchivoBloque(String msjTxt, String rutaOrigen, String rutaDestino, String extension,
			String filtro, int tamanoRegistro) {

		Path carpetaTemporal = null;
		try {
		for (int i = 1; i <= tamanoRegistro; i++) {
			
		
				String rutaOrigenFichero = rutaOrigen + filtro;
				String rutaDestinoFichero = rutaDestino + filtro;

				rutaOrigenFichero = rutaOrigenFichero + i + extension;
				rutaDestinoFichero = rutaDestinoFichero + i + extension;

				log.info(msjTxt + "Fichero Origen : " + rutaOrigenFichero);
				log.info(msjTxt + "Fichero Destino : " + rutaDestinoFichero);

				
					carpetaTemporal = Files.move(Paths.get(rutaOrigenFichero), Paths.get(rutaDestinoFichero));

					if (carpetaTemporal != null) {
						log.info(msjTxt + "archivo fue movido");
					} else {
						log.info(msjTxt + " Fallo al mover archivo");
					}

			}
		} catch (IOException e) {
			

			log.error(Constantes.ERROR_DETALLE ,e);
		}
	}

	public boolean moverArchivLocal(String mensajeTransaccion, File archivo, String directorio, String filtro) {
		boolean exito = false;
		
		try {

				if (archivo.getName().contains(filtro)) {
					if (archivo.renameTo(new File(directorio + archivo.getName()))) {

						log.info(mensajeTransaccion + " archivo " + archivo.getName() + " al directorio : "
								+ directorio);
						exito = true;
					} else {
						log.error(mensajeTransaccion + "Ocurrio un error al mover el archivo");
						
					}
				} else {
					log.info(mensajeTransaccion + archivo.getName() + "archivo no cumple con el filtro : " + filtro);
				}

		} catch (Exception e) {

		
			log.error(Constantes.ERROR_DETALLE,e);
		}

		return exito;
	}
	
	public String obtenerIdPlantilla(String archivo, String valorPlantillaCargo,String valorPlantillaSinCargo){
	    String idPlantilla=null;
	    
	        if( archivo.contains( Constantes.CON_CARGO ) ){
                idPlantilla= valorPlantillaCargo;
            }else{
                idPlantilla=valorPlantillaSinCargo;
            }
	    
	    return idPlantilla;
	
	}
	
	
	public String generarIdTransaccionFichero( String archivo){
	    String idTransaccion=null;
	    
	    Random valorRando=new Random();
	        idTransaccion= archivo.substring( Constantes.INT_CERO, archivo.indexOf( Constantes.GUION_ABAJO ))+valorRando.nextInt(Constantes.VALOR_RANDON_DEC);
	    return idTransaccion;
	}

	public boolean renombrarArchivo(String mensaje, File archivoAntiguo, File archivoRenombrado) {
		boolean exito = false;

		if (archivoAntiguo.renameTo(archivoRenombrado)) {
			exito = true;
			log.info(mensaje + " Se renombro archivo" + archivoRenombrado);
		} else {
			exito = false;
			log.info(mensaje + " Fallo al renombrar archivo");

		}

		return exito;
	}
	

}
