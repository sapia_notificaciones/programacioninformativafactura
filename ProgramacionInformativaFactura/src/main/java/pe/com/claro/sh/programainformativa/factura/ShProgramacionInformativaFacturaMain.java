package pe.com.claro.sh.programainformativa.factura;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import pe.com.claro.sh.programainformativa.factura.service.ProgramaNotificacionInformativaService;
import pe.com.claro.sh.programainformativa.factura.util.Constantes;
import pe.com.claro.sh.programainformativa.factura.util.PropertiesExternos;

@Component("ShProgramacionInformativaFacturaMain")
public class ShProgramacionInformativaFacturaMain {

	@Autowired
	private PropertiesExternos propertiesExternos;

	@Autowired
	private ProgramaNotificacionInformativaService programanotificacioninformativaservice;

	private static Logger log = Logger.getLogger(ShProgramacionInformativaFacturaMain.class);
	private static ApplicationContext objContextoSpring;

	public static void main(String[] args) {
		
		String idTransaction = args[Constantes.INT_CERO];
		String idPlantilla = args[Constantes.INT_UNO];
		String mensajeTransaccion = "[SH_INFORMATIVA_COBRANZA idTx=" + idTransaction + "] ";
		try {
			objContextoSpring = new ClassPathXmlApplicationContext(Constantes.CONFIG_PATH);
			ShProgramacionInformativaFacturaMain main = objContextoSpring.getBean(ShProgramacionInformativaFacturaMain.class);
			main.iniciarProceso( mensajeTransaccion,  idTransaction,idPlantilla);

		} catch (NoSuchBeanDefinitionException e) {
			
		
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			log.error("ERROR [NoSuchBeanDefinitionException] - : " , e);
			throw new BeanCreationException("Error NoSuchBeanDefinitionException" + e.getCause());
			
		}
	}

	public void iniciarProceso(String mensajeTransaccion, String idTransaccion,String idPlantilla) {
		String mensajeTx = mensajeTransaccion + " [iniciarProceso] - ";
		long startTime = System.currentTimeMillis();
		try {
			PropertyConfigurator.configure(propertiesExternos.vLog4JDir);
			programanotificacioninformativaservice.procesar(mensajeTx, idTransaccion, idPlantilla);
		} catch (BeanCreationException e) {
			log.error("ERROR [BeanCreationException] - : ", e);
			throw new BeanCreationException("Error BeanCreationException" + e.getCause());
		} finally {
			 log.info(mensajeTx + "---FIN--- Tiempo total de proceso(ms): " + (System.currentTimeMillis() - startTime) + " milisegundos.");
			log.info(mensajeTx + "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
			((ConfigurableApplicationContext)objContextoSpring).close();

		}
	}

}
