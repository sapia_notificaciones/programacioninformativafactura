package pe.com.claro.sh.programainformativa.factura.proxy;

import pe.com.claro.sh.programainformativa.factura.bean.notificaplantilla.NotificarPlantillaRequest;
import pe.com.claro.sh.programainformativa.factura.bean.notificaplantilla.NotificarPlantillaResponse;
import pe.com.claro.sh.programainformativa.factura.exception.WSException;

public interface NotificacionInformativa {
	
	NotificarPlantillaResponse  notificarPlantilla ( String mensajeTransaccion,String fecha, String  idTransaccion,
			String  userId,
			NotificarPlantillaRequest request) throws WSException;

	
	
}
